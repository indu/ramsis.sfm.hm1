# RT-RAMSIS HM1 SFM-Worker package

This package hosts the implementation of the HM1 Seismicity Forecast Model
Worker (SFM-Worker). It is part of RT-RAMSIS.

## Installation

The set-up is expected to be done on a windows machine, where the base HM1 c#
code is already installed. (Access settings.py to alter the default location of
this software and update if required, or pass in the location via the web service parameters
which have the same names and structures as in settings.py)

Create folder for cloned repository. (doesn't matter where, as long as user owns
the directory)

It is recommended to use Miniconda to set up a virtual environment for running
HM1. A standard python venv can also be used, but there is not access to as many
system dependencies for future-proofing the set-up.

Install the version required from this website:
https://docs.conda.io/en/latest/miniconda.html
open the installer and follow the instructions (all default options should be
fine)
Activate conda
```
Miniconda\Scripts\activate
conda create -n hm1 python=3.6
conda activate hm1
```
A postgres database is required. (postgres:11 has been used)
The installer can be found on this website:
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
Follow the instructions and set up a postgres user.
Set up a database called hm1 under a new user called ramsis (you can create a
password too). This is recommended to be done with psql, which might need
to be installed. (Other tools like pgadmin3 can be used)
```
psql
#(login as postres superuser using password set up on install)
create user hm1;
alter user hm1 with password 'new_password';
exit
psql -U hm1 -W
create database hm1;
exit
```

Follow the installation instructions of the `ramsis.utils` and
`ramsis.sfm.worker` namespace package, making sure the develop branches are
checked out. (This involves cloning the repositories
and running a pip install in the python environment)

Clone this repository and make sure develop is checked out
Then invoke:

```
$ pip install -e .
```

Start Web Service
Make sure you are in the virtual envirnment by activating conda hm1
Initialize the database, then start the web service (choose whatever port,
use the configured db name user and password)

```
ramsis-sfm-worker-db-init --logging-conf {path to logging}\ramsis.sfm.hm1\config\logging.conf postgresql://{username}:{password}@localhost:5432/{db name}
ramsis-sfm-worker-hm1 --logging-conf {path to logging}\ramsis.sfm.hm1\config\logging.conf --port 5007 postgresql://{username}:{password}@localhost:5432/{db name}
```

Test the web service from a browser
{ip or alias of computer}:5007/v1/HM1/runs

A list of job id's and statuses should be returned. (empty if data is newly
initialized)

You should be able to call the same from another networked computer.

Postman requests have been setup for the purpose of testing, please contact
laura.sarson@sed.ethz.ch for access. Examples of postman tests with the environment
variables are available for import from the Postman folder.




