# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
HM1 resource facilities.
"""
from flask_restful import Api

from ramsis.sfm.hm1 import settings
from ramsis.sfm.hm1.server import db
from ramsis.sfm.hm1.server.model_adaptor import ModelAdaptor
from ramsis.sfm.hm1.server.v1 import blueprint
from ramsis.sfm.hm1.server.v1.schema import SFMWorkerIMessageSchema, parser
from ramsis.sfm.worker.resource import (SFMRamsisWorkerResource,
                                        SFMRamsisWorkerListResource)

api_v1 = Api(blueprint)


class HM1API(SFMRamsisWorkerResource):

    LOGGER = 'ramsis.sfm.worker.hm1_api'


class HM1ListAPI(SFMRamsisWorkerListResource):
    """
    Concrete implementation of an asynchronous HM1 worker resource.

    :param model: Model to be handled by :py:class:`HM1ListAPI`.
    :type model: :py:class:`ramsis.sfm.worker.hm1.server.model.HM1Model`
    """
    LOGGER = 'ramsis.sfm.worker.hm1_list_api'

    def _parse(self, request, locations=('json', )):
        p = parser.parse(SFMWorkerIMessageSchema(), request,
                         locations=locations)
        return p


api_v1.add_resource(HM1API,
                    '{}/<task_id>'.format(settings.PATH_RAMSIS_HM1_SCENARIOS),
                    resource_class_kwargs={
                        'db': db})

api_v1.add_resource(HM1ListAPI,
                    settings.PATH_RAMSIS_HM1_SCENARIOS,
                    resource_class_kwargs={
                        'model': ModelAdaptor,
                        'db': db})
