# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
Service related schema facilities.
"""
from marshmallow import fields
from ramsis.sfm.worker import parser
from ramsis.sfm.worker.parser import ModelParameterSchemaBase, \
    create_sfm_worker_imessage_schema, UTCDateTime
from ramsis.sfm.worker.utils import SchemaBase
from ramsis.sfm.worker.utils import Positive

# FIXME(damb): Set default parameters.

# NOTE(damb): Since we (Philipp Kästli, Lukas Heiniger and me (damb)) decided
# keeping both model_parameters and the reservoir geometry under the model's
# responsability model_defaults configurerd at the worker's CLI are handled
# with higher preceedence than parameters configured from webserver clients.
# That is why parameters are not configured with required=True. (Parameters
# intentionally required are commented, bellow.)
#
# However, since the interface already existed, we still provide this
# interface.
#
class HM1ModelExternalSettingsSchema(SchemaBase):
    fraction_seismic_cloud = fields.Float()
    borehole_storage_coefficient = fields.Float()
    borehole_transmissivity = fields.Float()
    initial_storage_coeff = fields.Float()
    initial_transmissivity = fields.Float()
    deltat = fields.Float()
    mesh_extent = fields.Float()
    alfa_rate_of_growth = fields.Float()
    size_first_element = fields.Float()
    FileConvs = fields.List(fields.Float())
    FileCols = fields.List(fields.Integer())
    SkipLines = fields.Integer()


class HM1ModelCapsKDSettingsSchema(SchemaBase):
    RefinementRatio = fields.Integer()
    EnableScrOutput = fields.Boolean()


class HM1ModelSeedSettingsSchema(SchemaBase):
    slope_sigma1 = fields.Float()
    slope_sigma3 = fields.Float()
    sigma1_std = fields.Float()
    sigma3_std = fields.Float()
    fluid_density = fields.Float()
    gravity = fields.Float()
    inter_sigma1 = fields.Float()
    inter_sigma3 = fields.Float()
    b_vs_depth_range = fields.Bool()
    min_failure_pressure = fields.Float()
    stressdrop_coeff = fields.Float()
    cohesion_mean = fields.Float()
    friction_mean = fields.Float()
    PoissonDensity = fields.Float()


class HM1ModelParameterSchema(ModelParameterSchemaBase):
    # Test mode means that only the seed code, not HM1
    # is called and dummy data is used instead.
    # Purpose is for testing with postman and integration test
    # (test_hm1_data)
    hm1_test_mode = fields.Boolean()
    hm1_external_code_call = fields.String()
    hm1_training_magnitude_bin = fields.Float()
    hm1_training_epoch_duration = fields.Float()
    hm1_end_training = UTCDateTime('utc_isoformat')
    hm1_training_events_threshold = fields.Integer()
    hm1_threshold_magnitude = fields.Float()
    hm1_max_iterations = fields.Integer()
    hm1_differential_evolution_workers = fields.Integer()
    hm1_mc_bin_size = fields.Float()
    hm1_seed_settings = fields.Nested(HM1ModelSeedSettingsSchema)
    hm1_external_solution_settings = fields.Nested(
        HM1ModelExternalSettingsSchema)
    hm1_caps_kd_settings = fields.Nested(HM1ModelCapsKDSettingsSchema)
    hm1_calibration_start = fields.String()


#parser.HydraulicSampleScenarioSchema.\
#    _declared_fields['topflow_value'] = fields.Float(required=True)
#parser.HydraulicSampleScenarioSchema.\
#    _declared_fields['toppressure_value'] = Positive(required=True)
#parser.HydraulicSampleSchema.\
#    _declared_fields['topflow_value'] = fields.Float(required=True)
#
#parser.BoreholeSectionSchema.\
#    _declared_fields['holediameter_value'] = fields.Float(required=True)
#
#print("########################### parser :", parser.BoreholeSectionSchema._declared_fields['holediameter_value'])
SFMWorkerIMessageSchema = create_sfm_worker_imessage_schema(
    model_parameters_schema=HM1ModelParameterSchema)

parser = parser.parser
