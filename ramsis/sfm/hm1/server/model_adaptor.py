# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
HM1 model adaptor facilities.
"""
import os
import traceback
from osgeo import ogr
import numpy as np
from datetime import timedelta

from ramsis.sfm.worker import orm
from ramsis.sfm.worker.utils.misc import subgeom_reservoir_result, transform,\
    single_reservoir_result
from ramsis.sfm.worker.model_adaptor import (ModelAdaptor as _ModelAdaptor,
                                             ModelError, ModelResult)
from ramsis.sfm.hm1.core.utils import (obspy_catalog_parser, hydraulics_parser,
                                       translate_catalog_local_coords)
from ramsis.sfm.hm1.core import hm1_model
from ramsis.sfm.hm1.core.parser import remerge


class HM1Error(ModelError):
    """Base HM1 model error ({})."""

class ValidationError(HM1Error):
    """ValidationError ({})."""


class ModelAdaptor(_ModelAdaptor):
    """
    HM1 model implementation running the HM1 model code. The class wraps up
    model specifc code providing a unique interface.

    :param str reservoir_geometry: Reservoir geometry used by default.
        WKT format.
    :param dict model_parameters: Dictionary of model parameters used by
        default.
    """

    LOGGER = 'ramsis.sfm.worker.model_adaptor'

    NAME = 'HM1'
    DESCRIPTION = 'Alter description'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.model_defaults = kwargs
        self._default_reservoir = kwargs.get("reservoir")
        self._default_model_parameters = kwargs.get("model_parameters")

    def _run(self, **kwargs):
        """
        :param kwargs: Model specific keyword value parameters.
        """
        self.logger.debug(
            'Importing model specific configuration ...')
        # XXX(damb): Use a single view of model parameters sent by the client
        # and default model parameters injected while starting the service
        model_config, _ = remerge(
            [('defaults', self._default_model_parameters),
             ('overlay', kwargs.get('model_parameters', {}))],
            sourced=True)

        # XXX(damb): Default parameters are configured when starting the
        # worker.
        self.logger.debug(
            'Received model configuration: {!r}'.format(model_config))

        try:
            end_training = model_config['hm1_end_training']
        except KeyError:
            end_training = model_config['datetime_start']

        epoch_duration = model_config['epoch_duration']
        forecast_duration = (model_config['datetime_end']
                             - model_config['datetime_start']).total_seconds() # noqa

        if epoch_duration > forecast_duration:
            self.logger.info("The epoch duration is less than the "
                             "total time of forecast")
            epoch_duration = forecast_duration
            self.logger.info("The epoch duration has been set to the"
                             f"forcast duration: {forecast_duration} (s)")

        self.logger.debug('Importing reservoir geometry ...')
        try:
            reservoir_geom = kwargs['reservoir']['geom']
        except KeyError:

            self.logger.info('No reservoir exists.')
            reservoir_geom = self._default_reservoir

        self.logger.debug('Importing seismic catalog ...')
        x_vec = np.asarray(reservoir_geom["x"])
        y_vec = np.asarray(reservoir_geom["y"])
        reservoir_height = kwargs["well"]["altitude_value"]
        z_vec = np.asarray(reservoir_geom["z"]) - reservoir_height
        print("z_vec", z_vec)
        if any(len(lst) != len(x_vec) for lst in [y_vec, z_vec]):
            raise HM1Error("the length of input reservoir edges"
                           " must be equal.")
        forecast_edges = [x_vec, y_vec, z_vec]
        try:
            catalog = obspy_catalog_parser(
                kwargs["seismic_catalog"]["quakeml"])
            print("input catalog", catalog)
            self.logger.info(
                f'Received seismic catalog with {len(catalog)} event(s).')
        except KeyError:
            self.logger.warning(
                'Received catalog without quakeml key.')
            raise ValidationError('No quakeml key found for catalog.')

        catalog = self._filter_catalog(catalog, reservoir_geom,
                                       kwargs.get("spatialreference"),
                                       kwargs.get("referencepoint"))
        if model_config['hm1_training_events_threshold'] < 5:
            raise IOError(
                "The HM1 model training event threshold parameter must "
                "be greater than 5 to run.")

        min_reservoir_depth = -max(reservoir_geom["z"])
        max_reservoir_depth = -min(reservoir_geom["z"])
        self.logger.info('Filtering catalog on depth {} {}'.format(min_reservoir_depth, max_reservoir_depth))
        catalog = self.filter_catalog_depth(
            catalog, min_reservoir_depth, max_reservoir_depth)
        self.logger.info('After filtering on depth, seismic catalog '
                         f'contains: {len(catalog)} events')
        # Translate catalog to local coordinates based on well data
        print("before translate catalog", catalog)
        catalog = translate_catalog_local_coords(kwargs["well"], catalog)
        print("after translate catalog", catalog)

        # Adding in hack as I think the model takes a depth instead of up
        #catalog['up'] = -catalog['up']
        print("after catalog 'up' reversal", catalog)

        self.logger.debug('Importing real hydraulic data ...')
        try:
            # The section coordinates are in frame of reference of
            # the well head.
            (hydraulics, well_radius,
             section_top_coordinates,
             section_bottom_coordinates) = hydraulics_parser(kwargs["well"])

            self.logger.info(
                'Received borehole ({} hydraulic sample(s)).'.format(
                    len(hydraulics)))
        except KeyError as err:
            raise HM1Error(
                f'Received borehole without hydraulic samples. {err}')

        self.logger.debug('Importing scenario hydraulic data...')
        try:
            hydraulics_scenario, _, _, _ = hydraulics_parser(
                kwargs["scenario"]["well"], scenario=True)
            self.logger.info(
                'Received scenario ({} hydraulic sample(s)).'.format(
                    len(hydraulics_scenario)))
        except KeyError:
            raise HM1Error(
                'Received scenario that does not meet criteria.')

        try:
            calibration_start = model_config['hm1_calibration_start']
            self.logger.info(f'Filtering hydraulics by calibration date: {calibration_start}')
            hydraulics = hydraulics.loc[calibration_start:end_training]
        except KeyError:
            pass
        self.logger.debug('Checking training period...')
        start_time_hydraulics = hydraulics.sort_index().index[0].\
            to_pydatetime()
        training_epoch_duration = (end_training - start_time_hydraulics).\
            total_seconds()
        try:
            training_epoch_duration = model_config[
                'hm1_training_epoch_duration']
        except KeyError:
            self.logger.info("No training_epoch_duration is set.")

            self.logger.info("Setting training_epoch_duration to: "
                             f"{training_epoch_duration}.")
        if training_epoch_duration <= 0.:
            raise HM1Error("End of training set to before the "
                           "first training data hydraulic sample.")
        # Filter catalog by time
        catalog = catalog.loc[start_time_hydraulics:end_training]
        if len(catalog) < model_config['hm1_training_events_threshold']:
            raise IOError(
                "The length of the catalog is not sufficient for the model: "
                f"{len(catalog)}, the number of events must > "
                f"{model_config['hm1_training_events_threshold']}.")

        self.logger.info(
            "Number of events are filtering catalog is "
            f"{len(catalog)} for time {start_time_hydraulics}:{end_training}")

        self.logger.info("Calling the HM1 model...")
        print("Inputs to model"
               "\n", "end training", end_training,
               "\n", "training epoch duration", training_epoch_duration,
               "\n", "training magnitude bin", model_config['hm1_training_magnitude_bin'],
               "\n", "training events threshold", model_config['hm1_training_events_threshold'],
               "\n", "forecast starttime", model_config['datetime_start'],
               "\n", "forecast end time", model_config['datetime_end'],
               "\n", "epoch duration", epoch_duration,
               "\n", "forecast edges", forecast_edges,
               "\n", "max iterations", model_config['hm1_max_iterations'],
               "\n", "differential evolution workers", model_config['hm1_differential_evolution_workers'],
               "\n", "seed settings", model_config['hm1_seed_settings'],
               "\n", "external solution settings", model_config['hm1_external_solution_settings'],
               "\n", "caps kd settings", model_config['hm1_caps_kd_settings'],
               "\n", "mc bin size", model_config['hm1_mc_bin_size'],
               "\n", "well radius", well_radius,
               "\n", "section top cordinates", list(section_top_coordinates),
               "\n", "bottom coordinates", list(section_bottom_coordinates),
               "\n", "test mode", model_config['hm1_test_mode'],
               "\n", "external code call", model_config['hm1_external_code_call'],
               "\n", "base dir", model_config['hm1_base_dir'])
        #with open(os.path.join("C:/Users/ramsis/repos", 'catalog.csv'), 'w') as catalog_file:
        #    catalog.to_csv(catalog_file)
        #with open(os.path.join("C:/Users/ramsis/repos", 'hydraulics.csv'), 'w') as catalog_file:
        #    hydraulics.to_csv(catalog_file)
        #with open(os.path.join("C:/Users/ramsis/repos", 'scenario.csv'), 'w') as catalog_file:
        #    hydraulics_scenario.to_csv(catalog_file)
        try:
            forecast = hm1_model.exec_model(
                catalog,
                hydraulics,
                hydraulics_scenario,
                end_training,
                training_epoch_duration,
                model_config['hm1_training_magnitude_bin'],
                model_config['hm1_training_events_threshold'],
                model_config['datetime_start'],
                model_config['datetime_end'],
                epoch_duration,
                forecast_edges,
                model_config['hm1_max_iterations'],
                model_config['hm1_differential_evolution_workers'],
                model_config['hm1_seed_settings'],
                model_config['hm1_external_solution_settings'],
                model_config['hm1_caps_kd_settings'],
                model_config['hm1_mc_bin_size'],
                well_radius,
                list(section_top_coordinates),
                list(section_bottom_coordinates),
                model_config['hm1_test_mode'],
                model_config['hm1_external_code_call'],
                model_config['hm1_base_dir'])
        except Exception:
            err = traceback.print_exc()
            forecast = None
        else:
            err = False
        if err:
            raise
        # Quirk of set-up means that we need to raise another error.
        if not forecast:
            raise HM1Error('Error raised in HM1 model')

        dttimes_seconds, number_events, max_mw, mc, b, a = forecast
        dttimes = [model_config['datetime_start'] + # noqa
                   timedelta(seconds=i) for i in dttimes_seconds]
        # b single value
        # a value (time bins, x, y, z) the order of the coords not confirmed
        # mc single value
        # number events (a derived from this, same as a)
        self.logger.debug("Result received from HM1 model.")

        subgeoms = subgeom_results(self, dttimes, number_events, b,
                                   mc, a, reservoir_geom)

        reservoir = single_reservoir_result(reservoir_geom,
                                            [],
                                            subgeoms=subgeoms)
        return ModelResult.ok(
            data={"reservoir": reservoir},
            warning=self.stderr if self.stderr else self.stdout)

    def _filter_catalog(self, catalog, geom, spatial_reference,
                        referencepoint):
        # Apply the reference point offset to the resrevoir.
        print(catalog)
        min_reservoir_height = min(geom["z"])
        max_reservoir_height = max(geom["z"])
        min_reservoir_x = min(geom["x"]) + referencepoint["x"]
        max_reservoir_x = max(geom["x"]) + referencepoint["x"]
        min_reservoir_y = min(geom["y"]) + referencepoint["y"]
        max_reservoir_y = max(geom["y"]) + referencepoint["y"]
        print("transform, max", transform(min_reservoir_x, min_reservoir_y, spatial_reference), transform(max_reservoir_x, max_reservoir_y, spatial_reference))
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(*transform(min_reservoir_x,
                                 min_reservoir_y,
                                 spatial_reference))
        ring.AddPoint(*transform(min_reservoir_x,
                                 max_reservoir_y,
                                 spatial_reference))
        ring.AddPoint(*transform(max_reservoir_x,
                                 max_reservoir_y,
                                 spatial_reference))
        ring.AddPoint(*transform(max_reservoir_x,
                                 min_reservoir_y,
                                 spatial_reference))
        poly = ogr.Geometry(ogr.wkbPolygon)
        poly.AddGeometry(ring)
        poly.CloseRings()
        poly.FlattenTo2D()
        self.logger.info('Filtering catalog on latitude and longitude...{}'.
                         format(poly.Area()))

        catalog = catalog[catalog.apply(self.filter_catalog_ogr, axis=1,
                                        args=[poly])]
        self.logger.info('After filtering on reservoir, seismic catalog '
                         f'contains: {len(catalog)} events')
        return catalog

    @staticmethod
    def filter_catalog_ogr(catalog, geom):
        event_loc = ogr.CreateGeometryFromWkt(
            f"POINT ({catalog['lon']} {catalog['lat']})")
        return geom.Contains(event_loc)

    @staticmethod
    def filter_catalog_depth(catalog, min_depth, max_depth):
        """
        The obspy filtering does not work for the third dimension
        for unexplained reasons (sfcgal library linked to gdal should support
        3D functionality, but this does not solve the problem)

        As a simplified work-around, the depth will be filtered by the
        geometry envelope, which means that the events with depth
        outside the minimum depth and maximum depth in the reservoir
        will be excluded. The expected geometry is of a cuboid.

        :param catalog: Catalog of seismic events
        :type catalog: pandas DataFrame
        :param geom: geometry of reservoir defined by a polyhedral suface
            of a cuboid shape.
        :type geom: ogr Geometry

        :rtype: pandas DataFrame
        """

        catalog = catalog[catalog.depth < max_depth]
        catalog = catalog[catalog.depth > min_depth]
        return catalog


def create_samples(self, dttimes, n_subgeom, b,
                   mc, a_subgeom, forecast_edges):
    """
    Creates a list of ModelResultSamples for a reservoir
    based on a list of datetimes.
    The first sample will get thrown away.

    :param dttimes: List of datetime intervals that have been forecast
        for, each value is the break between the last and new interval
    :param n_subgeom: Output from HM1 model as a 1d array (time)
    :param b: Output from HM1 single b value is input.
    :param mc: As required by the GR relation, mc gives the minimum
        threshold allowed in the catalog to be used. Float.
    :param a_subgeom: Output from HM1 as defined by GR relation, 1d array
        (time) is defined for each time interval
    :param forecast_edges: list of arrays for x, y, z coordinates defining
        edges of subgeometries.
    """
    start_date = None
    samples = []
    for d, dttime in enumerate(dttimes[:-1]):
        if not start_date:
            start_date = dttime
        if not n_subgeom[d]:
            start_date = dttimes[d+1]
            continue

        samples.append(orm.ModelResultSample(
            starttime=start_date,
            endtime=dttimes[d+1],
            b_value=b,
            a_value=a_subgeom[d],
            mc_value=mc,
            numberevents_value=n_subgeom[d]))
        start_date = dttimes[d+1]
    return samples

def subgeom_results(self, dttimes, number_events, b,
                    mc, a, reservoir_geom):
    """
    Loop through subgeometry volumes given by reservoir_geom
    and create a reservoir with a result time series for all.
    The subgeometry reservoirs are then collected under a parent
    reservoir which references the entire size of reservoir the
    model has used to calculate over.

    :param dttimes: List of datetime intervals that have been forecast
        for, each value is the break between the last and new interval
    :param number_events: Output from HM1 model as a 4d array
        (time, x, y, z)
    :param b: Output from HM1 as 1d array (time) a single b value is input
        for each time interval.
    :param mc: As required by the GR relation, mc gives the minimum
        threshold allowed in the catalog to be used. Float.
    :param a: Output from HM1 as defined by GR relation, 4d array
        (time, x, y, z) is defined for each time interval
    :param reservoir_geom: list of arrays for x, y, z coordinates defining
        edges of subgeometries.
    """
    number_events = np.nan_to_num(number_events, 0.)
    print("dttimes", dttimes, number_events.shape, a.shape)
    subgeoms = []

    for i in range(len(reservoir_geom["x"]) - 1):
        for j in range(len(reservoir_geom["y"]) - 1):
            for k in range(len(reservoir_geom["z"]) - 1):
                try:
                    a_subgeom = a[:, i, j, k]
                    n_subgeom = number_events[:, i, j, k]
                except IndexError:
                    raise HM1Error(
                        "output of hm1 does not match reservoir size")
                if not n_subgeom.any():
                    continue
                samples = create_samples(self, dttimes, n_subgeom,
                                         b, mc, a_subgeom, reservoir_geom)

                if not samples:
                    continue
                reservoir = subgeom_reservoir_result(
                    reservoir_geom["x"][i],
                    reservoir_geom["x"][i + 1],
                    reservoir_geom["y"][j],
                    reservoir_geom["y"][j + 1],
                    reservoir_geom["z"][k],
                    reservoir_geom["z"][k + 1],
                    samples)
                subgeoms.append(reservoir)
    return subgeoms
