# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
General purpose configuration constants.
"""

from ramsis.sfm.worker import settings

RAMSIS_WORKER_HM1_ID = 'HM1'
RAMSIS_WORKER_HM1_PORT = 5000
RAMSIS_WORKER_HM1_CONFIG_SECTION = 'CONFIG_SFM_WORKER_HM1'

PATH_RAMSIS_HM1_SCENARIOS = ('/' + RAMSIS_WORKER_HM1_ID + # noqa
                             settings.PATH_RAMSIS_WORKER_SCENARIOS)

# TODO(damb): Configure meaningful defaults
RAMSIS_WORKER_SFM_DEFAULTS = {
    # XXX(damb): Reservoir description (format=WKT, srid=4326)
    "reservoir": {"geom": {"x": [-2000, 0, 2000],
                           "y": [-2000, 0, 2000],
                           "z": [-4000, -2000, 0]}},

    "model_parameters": {
        "hm1_test_mode": False,
        "hm1_base_dir": "C:\RAMSIS\Worker_New\Simulation_Test",
        "hm1_external_code_call": "Bin\GES.RAMSIS.SSM.Simulation.Main.exe", # noqa
        # XXX(damb): Local spatial reference system (SRS) spatial data is
        # transformed into (format=Proj4)
        #"epoch_duration": 14400.0,
        # Need to make sure that all of these are used below
        "hm1_training_magnitude_bin": 0.2,
        "hm1_training_threshold_magnitude": 2.6,
        "hm1_training_events_threshold": 5.0,
        "hm1_max_iterations": 100,
        "hm1_differential_evolution_workers": -1,
        "hm1_mc_bin_size": 0.005,
        "hm1_seed_settings": {
            "slope_sigma1": 0.037,
            "slope_sigma3": 0.015,
            "sigma1_std": 10,
            "sigma3_std": 10,
            "fluid_density": 985,
            "gravity": 9.81,
            "inter_sigma1": 0,
            "inter_sigma3": 0,
            "b_vs_depth_range": True,  # b=a+m*z
            # I think these do not need to be set as they are set in
            # update_seeds_settings
            #"a": 2.,
            #"m": [0, 0, -0.000167],
            #"Mag_Completeness": 0.9,
            "min_failure_pressure": 0.01,
            "stressdrop_coeff": 3.,
            "cohesion_mean": 2,
            "friction_mean": 0.6,
            "PoissonDensity": 1.e-6},
        "hm1_external_solution_settings": {
            # Order of columns for numpy array pressure, time, not sure the
            # last one.
            "FileCols": [2, 0, 1],
            # Coefficients of pressure, time, unknown col
            "FileConvs": [1.e-6, 24.*3600., 1.],
            # Number of lines to skip when reading Tecplot files
            "SkipLines": 6,
            # Lw1/2 seem to be the coordinates for the top and bottom
            # of the cylinder. Where should these come from?
            #"Lw1": [0.,0.,5000.],
            #"Lw2": [-31., 33., 4618.3],
            # These should come from the hydraulic data
            #"Lw1": [0.,0.,500.],
            #"Lw2": [-31., 33., 318.3],
            "fraction_seismic_cloud": 1.85,
            "borehole_storage_coefficient": 0.02097,
            "borehole_transmissivity": 1000,
            "initial_storage_coeff": 3.4E-05,
            "initial_transmissivity": 7.43E-08,
            # This is the 'calibration time' for Dieter's model.
            # It should come from another input.
            # I think this should probably be the full time for calibration,
            # but the catalog is much larger than this, is it not?
            #"time_discretization": 7200,
            # Input to Dieter's HM1 code as a parameter
            # The smaller the time step, the more rapidly the adaption of the
            # calibration parameters.
            "deltat": 60,
            #Should come from hydraulic data
            #"well_radius": 0.118,
            # This should come from the geometry of the well? Not sure.
            "mesh_extent": 1000,
            "alfa_rate_of_growth": 5,
            "size_first_element": 0.01},

        "hm1_caps_kd_settings": {
            "RefinementRatio": 5,
            "EnableSrcOutput": True}}}
