#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 12:12:26 2019

@author: dkarvoun
"""
import numpy as np
import pandas as pd
import os
import KD_CAPS as DK
from datetime import datetime
import copy

#from scipy import misc,  stats
from my_DK_RAMSIS import Eszter_LL_score, Calibrate_Observed_GR_Law_EM1_like
from scipy.optimize import differential_evolution

class HM1:
    def __init__(self, StartTime=0, 
                 Recenter=np.zeros(3), 
#                 width=1., 
                 time_col='time', 
                 flow_col='flow', 
                 press_col='pressure', 
                 source_times_col='ts', 
                 hypocenter_cols=['Xs','Ys','Zs'], 
                 Mw_cols='Mw', 
                 CalibrationCmd = 'echo "Running CalibrationCmd"',#'.\Bin\GES.RAMSIS.SSM.Simulation.Main.exe --Operation CalibratePress --InputParaFile input_para_CalibratePress.csv', 
                 ForecastCmd='echo "Running ForecastCmd"',#'.\Bin\GES.RAMSIS.SSM.Simulation.Main.exe --Operation ForecastPress --InputParaFile input_para_ForecastPress.csv',
                 Settings=DK.Exemplary_Configuration_Dictionaries()):
        self.StartTime      =StartTime
        self.Recenter       =Recenter
        self.myCcmd         =CalibrationCmd 
        self.myFcmd         =ForecastCmd
        self.MyColumns      =[time_col, flow_col, press_col]
        self.ts             =source_times_col
        self.Xs             =hypocenter_cols
        self.Mw             =Mw_cols
        self.SeedsSettings  =Settings[0]
        self.HM1Settings    =Settings[1]
        self.CAPSSettings   =Settings[2]
#        self.width          =width
        
        self.Read_Pressure_Forecast_From=os.path.join('Tecplot', 'Press_Dist_Forecast.dat')
        self.Read_Pressure_Calibration_From=os.path.join('Tecplot','Press_Dist_Calib.dat')
        self.HM1Settings["FileNameSolutions"]=self.Read_Pressure_Calibration_From
        self.Read_HM1_Cloud=os.path.join('CSV','CloudPara.csv')
        self.Save_Past_Injections=['Q_Measured_Calibrate.SED_CSV.csv','Time(UTC); Q(l/min)\n',
                                   '%d.%m.%Y %H:%M:%S','\n']
        self.Save_Past_WellPress =['P_Measured.SED_CSV.csv','Time(UTC); p(Pa)\n',
                                   '%d.%m.%Y %H:%M:%S','\n']
        self.Save_Past_Catalog   =['SeismicCatalog_Measured.SED_CSV.csv','Time;Offset-X(m);Offset-Y(m);Offset-Z(m);Local magnitude\n',
                                   '%d.%m.%Y %H:%M:%S','\n']
        self.Save_Future_Injection=['Q_Measured_Forecast.SED_CSV.csv','Time(UTC); Q(l/min)\n',
                                   '%d.%m.%Y %H:%M:%S','\n']
        self.Training_Well_Logs=None
        self.Training_Catalog  =None   ## coordinates as Dieter needs them
        self.CostFunction=None
        self.LL_Edges=None
        self.LL_Moments=None
        self.Training_MaxPressure=None
        self.Training_XYZ=None
        self.Training_Volume=None
        self.OptimizeKeys=["stressdrop_coeff", "PoissonDensity", "sigma1_std", "min_failure_pressure"]
        self.bounds      =[(Settings[0]["stressdrop_coeff"]*0.5,Settings[0]["stressdrop_coeff"]*1.5), (-10, 10),
                           (0.,Settings[0]["sigma1_std"]), (1.e-4,Settings[0]["min_failure_pressure"])]
        self.logscale    =[False, True, False, False]
        self.disp=True
        self.maxiter=100
        self.seed=42
        self.workers=-1
        
#%%
    
    

    def Update_Seeds_Settings(self):
        Mc, b_value =  Calibrate_Observed_GR_Law_EM1_like(self.Training_Catalog, 0.005, self.Mw)
        self.SeedsSettings["Mag_Completeness"]=Mc
        self.Training_Catalog=self.Training_Catalog[self.Training_Catalog[self.Mw]>=Mc]
        self.SeedsSettings["a"], self.SeedsSettings["m"]= [b_value, np.zeros(3)]
        if (self.SeedsSettings["b_vs_depth_range"]):
            self.SeedsSettings["a"], self.SeedsSettings["m"]=DK.b_value_Lasso_calibration(np.array(self.Training_Catalog[self.Xs].values),np.array(self.Training_Catalog[self.Mw]) , bins=100)
  
    def Update_HM1_Settings(self):
        XYZ=np.array(self.Training_Catalog[self.Xs])
        Cloud_Shape = pd.read_csv(self.Read_HM1_Cloud, sep=';', index_col=0)
        Center=[Cloud_Shape['Value']['x0'], Cloud_Shape['Value']['y0'], Cloud_Shape['Value']['z0']]
        NormalVec=[Cloud_Shape['Value']['n_x'], Cloud_Shape['Value']['n_y'], Cloud_Shape['Value']['n_z']]
        NormalVec=NormalVec/np.linalg.norm(NormalVec)
        widths=(XYZ-Center).dot(NormalVec)
        
        width=np.abs(np.percentile(widths,95)-np.percentile(widths,5))
        self.HM1Settings['Lw1']=Center+NormalVec*(0.5*width)
        self.HM1Settings['Lw2']=Center-NormalVec*(0.5*width)

    def LL_score(self, x):
        y                    =self.CostFunction.y
        log_factorial_y      =self.CostFunction.logyfact
        TestSettings         =copy.deepcopy(self.SeedsSettings)
        for i in range(len(x)):
            TestSettings[self.OptimizeKeys[i]]=x[i]
            if (self.logscale[i]):
                TestSettings[self.OptimizeKeys[i]]=np.power(10.,x[i])
        y1=DK.Forecast_BinnedRate_only(self.LL_Moments[1:], self.LL_Edges, self.Training_MaxPressure, self.Training_XYZ, self.Training_Volume, TestSettings)
        y1=y1.ravel()
        y1[np.where(y1<=0.)]=1.e-20
        LL=np.sum(y*np.log10(y1)-y1-log_factorial_y)/float(len(y1))
        return -LL
            
    def Calibrate_CAPS(self):   
        self.Update_HM1_Settings()
        self.Update_Seeds_Settings()
        self.CostFunction=Eszter_LL_score(np.array(self.Training_Catalog[np.append(self.ts, self.Xs)]), bins=5)
        self.LL_Edges=self.CostFunction.edges[1:4]
        self.LL_Moments=self.CostFunction.edges[0]
        self.Training_MaxPressure, self.Training_XYZ, self.Training_Volume=DK.Interpolate_Maximum_Pressure(self.LL_Moments, self.LL_Edges, self.HM1Settings, self.CAPSSettings)
        result = differential_evolution(self.LL_score, self.bounds, disp=self.disp, maxiter=self.maxiter, seed=self.seed)
        for i in range(len(result.x)):
            self.SeedsSettings[self.OptimizeKeys[i]]=result.x[i]
            if (self.logscale[i]):
                self.SeedsSettings[self.OptimizeKeys[i]]=np.power(10.,result.x[i])
        # correct Poisson Density for systematic errors mismatches due to HM1
        my_y=DK.Forecast_BinnedRate_only(self.LL_Moments[-1:], self.LL_Edges, self.Training_MaxPressure[-1:], self.Training_XYZ, self.Training_Volume, self.SeedsSettings)     
        NewPoisson=self.SeedsSettings['PoissonDensity']*len(self.Training_Catalog)/np.sum(my_y)
        print('Increase max LL Poisson density by a factor of ', NewPoisson, ' to correct systematic errors.')
        self.SeedsSettings['PoissonDensity']=NewPoisson
        return


    def Save_HM1_Well_Logs(self):
        DateHM1=np.array(self.Training_Well_Logs[self.MyColumns[0]].values)+self.StartTime
        Q_HM1=np.array(self.Training_Well_Logs[self.MyColumns[1]].values)
        P_HM1=np.array(self.Training_Well_Logs[self.MyColumns[2]].values)
        with open(self.Save_Past_Injections[0], 'w') as file:
            Q_HM1[np.isnan(Q_HM1)]=0
            file.write(self.Save_Past_Injections[1])
            for t in range(len(DateHM1)):
                file.write(str(datetime.fromtimestamp(DateHM1[t]).strftime(self.Save_Past_Injections[2]))+";"+str(Q_HM1[t]*6.e4)+self.Save_Past_Injections[3])
#        file.write(str(datetime.fromtimestamp(DateHM1[t]))+";"+str(Q_HM1[t]*6.e4)+"\n")
        with open(self.Save_Past_WellPress[0], 'w') as file:
            P_HM1[np.isnan(P_HM1)]=0
            file.write(self.Save_Past_WellPress[1])
            for t in range(len(DateHM1)):
                file.write(str(datetime.fromtimestamp(DateHM1[t]).strftime(self.Save_Past_WellPress[2]))+";"+str(P_HM1[t])+self.Save_Past_WellPress[3])        
        return

    def Save_HM1_Catalog(self):
        with open(self.Save_Past_Catalog[0], 'w') as file:
            file.write(self.Save_Past_Catalog[1])
            for t in range(len(self.Training_Catalog)):
                my_string=str(datetime.fromtimestamp(self.Training_Catalog.iloc[t][self.ts]+self.StartTime).strftime(self.Save_Past_Catalog[2]))+"; "
                my_string=my_string+str(self.Training_Catalog.iloc[t][self.Xs[0]])+"; "
                my_string=my_string+str(self.Training_Catalog.iloc[t][self.Xs[1]])+"; "
                my_string=my_string+str(self.Training_Catalog.iloc[t][self.Xs[2]])+"; "
                my_string=my_string+str(self.Training_Catalog.iloc[t][self.Mw])+self.Save_Past_Catalog[3]
                file.write(my_string)        
        return
    
    def fit(self, HydraulicLogs_coarse_pd, catalog_incomplete_df, deep=True):
        self.HM1Settings["FileNameSolutions"]=self.Read_Pressure_Calibration_From
        self.Training_Well_Logs=HydraulicLogs_coarse_pd.copy(deep)
        self.Save_HM1_Well_Logs()
        self.Training_Catalog  =catalog_incomplete_df.copy(deep)
        self.Training_Catalog[self.Xs]=self.Training_Catalog[self.Xs]+self.Recenter
        self.Save_HM1_Catalog()
#
        print("calling ",self.myCcmd)
        os.system(self.myCcmd) ## calibrate flow model
        self.Calibrate_CAPS()
        self.HM1Settings["FileNameSolutions"]=self.Read_Pressure_Forecast_From
        f = open("Calibrated_CAPS.txt","w")
        f.write( str(self.SeedsSettings) )
        f.close()
        return
    
    def Recenter_Edges(self,Forecasts_Edges):
        print("Recentering edges by", self.Recenter)
        my_edges=np.copy(Forecasts_Edges)
        my_edges[0]+=self.Recenter[0]
        my_edges[1]+=self.Recenter[1]
        my_edges[2]+=self.Recenter[2]
        return my_edges 
    
    def Save_HM1_Forecast_Injection(self, PlannedStrategy, dt_mean):
        compressed_DateHM1=np.sort(np.array(PlannedStrategy[:,0])+self.StartTime)
        Q_HM1  =np.array(PlannedStrategy[:,1])
        DateHM1=np.arange(compressed_DateHM1[0], compressed_DateHM1[-1], dt_mean)
        DateHM1=np.concatenate( (compressed_DateHM1, DateHM1) )
        DateHM1=np.sort( np.unique(DateHM1) )
        with open(self.Save_Future_Injection[0], 'w') as file:
            Q_HM1[np.isnan(Q_HM1)]=0
            file.write(self.Save_Future_Injection[1])
            counter=0
            for t in range(len(DateHM1)):
                if(compressed_DateHM1[counter]<DateHM1[t]):
                    counter=np.searchsorted(compressed_DateHM1, DateHM1[t])
                file.write(str(datetime.fromtimestamp(DateHM1[t]).strftime(self.Save_Future_Injection[2]))+";"+str(Q_HM1[counter]*6.e4)+self.Save_Future_Injection[3])           
        return
        
    def predict(self, PlannedStrategy, Forecasts_Moments, Forecasts_Edges=None, dt_mean=60.):
        my_HM1_Edges=self.Recenter_Edges(Forecasts_Edges)
        self.HM1Settings["FileNameSolutions"]=self.Read_Pressure_Calibration_From
        MaxPressure_past, My_Working, My_Cells_Volume=DK.Interpolate_Maximum_Pressure(Forecasts_Moments, my_HM1_Edges, self.HM1Settings, self.CAPSSettings)
        self.HM1Settings["FileNameSolutions"]=self.Read_Pressure_Forecast_From
        self.Save_HM1_Forecast_Injection(PlannedStrategy, dt_mean)
        os.system(self.myFcmd)
        MaxPressure, My_Working_XYZ, My_Cells_Volume=DK.Interpolate_Maximum_Pressure(Forecasts_Moments, my_HM1_Edges, self.HM1Settings, self.CAPSSettings)
        MaxPressure=np.maximum(MaxPressure_past,MaxPressure)
        NormalizedBinnedSeismicity, TotalEvents, TotalBvalue=DK.Forecast_TotalEvents_Bvalue(Forecasts_Moments, my_HM1_Edges, MaxPressure, My_Working_XYZ, My_Cells_Volume,self.SeedsSettings)  
        return TotalEvents, TotalBvalue
    
        self.HM1Settings["FileNameSolutions"]=self.Read_Pressure_Forecast_From
        my_HM1_Edges=self.Recenter_Edges(Forecasts_Edges)
        self.Save_HM1_Forecast_Injection(PlannedStrategy, dt_mean)
        print("calling ",self.myFcmd)
        os.system(self.myFcmd)
        MaxPressure, My_Working_XYZ, My_Cells_Volume=DK.Interpolate_Maximum_Pressure(Forecasts_Moments, my_HM1_Edges, self.HM1Settings, self.CAPSSettings)
        NormalizedBinnedSeismicity, TotalEvents, TotalBvalue=DK.Forecast_TotalEvents_Bvalue(Forecasts_Moments, my_HM1_Edges, MaxPressure, My_Working_XYZ, My_Cells_Volume,self.SeedsSettings)  
        return TotalEvents, TotalBvalue


  #
#    def score(self, X, y, Forecasts_Edges):
#        yfact=misc.factorial(y)
#        y1=self.predict(X)
#        LL=y*np.log(y1)-y1-np.log(yfact)
#        return np.sum(LL)  
#    def Estimate_failure_pressure(self, XYZ, ts):
#        My_PressureFunction, External_timesteps = DK.Interpolator_Of_External_Pressure_Solution_Ptx(self.HM1Settings["FileNameSolutions"], self.HM1Settings["FileCols"], self.HM1Settings["FileConvs"], self.HM1Settings["SkipLines"])
#        R= DK.From3DCartesian2_1DCylindrical(XYZ, np.array([self.HM1Settings["Lw1"]]), np.array([self.HM1Settings["Lw2"]]))
#        Pf=np.array( My_PressureFunction(R, self.Training_Catalog[self.ts].values) )
#        return Pf    
        
#    def Make_My_Sets_of_Seeds(self, My_Working_XYZ):
#        CAPS_Z=np.array([My_Working_XYZ[:,2]]).transpose()
#        CAPS_Coh= self.SeedsSettings["cohesion_mean"]
#        CAPS_fr=self.SeedsSettings["friction_mean"]
#        CAPS_P0=self.SeedsSettings["min_failure_pressure"]
#        CAPS_stressdrop_coeff=self.SeedsSettings["stressdrop_coeff"]
#        CAP_dSdZ=np.array([[self.SeedsSettings["slope_sigma1"], self.SeedsSettings["slope_sigma3"]]])
#        CAPS_Phydro=CAPS_Z*(self.SeedsSettings["gravity"]*985.*1.e-6)
#        CAPS_MeanStresses=CAPS_Z*CAP_dSdZ+np.array([self.SeedsSettings["inter_sigma1"], self.SeedsSettings["inter_sigma3"]])
#        CAPS_StdStress=CAPS_MeanStresses*np.array([self.SeedsSettings["sigma1_std"]/100., self.SeedsSettings["sigma3_std"]/100.])
#        CAPS_Seeds=NarrayNormalSeeds(CAPS_MeanStresses, CAPS_StdStress, CAPS_fr, CAPS_Coh, CAPS_stressdrop_coeff)
#        return CAPS_Seeds
    
#    def Locate_TrunNorm_Median_of_Subspace(Up_2_X, Xmean, Xstd, Xmin=-1.e20, Xmax=1.e20):
#        Up_To_F=DK.TruncNormCDF(Up_2_X, Xmean, Xstd, Xmin, Xmax)
#        Median_Up_2_X=DK.TruncNormCDF_inv(Up_To_F*0.5, Xmean, Xstd, Xmin, Xmax)
#        return Median_Up_2_X
#    
#    def sigma_std_score(sigma_std, true_median):
#        score=Locate_TrunNorm_Median_of_Subspace-true_median
#        return score
        
#    def loglikelihood( y1,y,edges,yfact=None):
#        if (yfact==None):
#            yfact=misc.factorial(y)
#        LL=y*np.log(y1)-y1-np.log(yfact)
#        return np.sum(LL)
        
    
#        My_XYZ=np.array(self.Training_Catalog[self.Xs])+self.Recenter
#        Pf=self.Estimate_failure_pressure(My_XYZ, self.Training_Catalog[self.ts].values)
#        self.SeedsSettings["min_failure_pressure"]=np.min(Pf)
#        Pf_1stTrig, Pf_bins, binnumber=stats.binned_statistic_dd(My_XYZ[Pf>0.],Pf[Pf>0.], statistic='min', bins=20)
#        Pf_med, Pf_subrange=[np.nanmedian(Pf_1stTrig), (np.nanmax(Pf_1stTrig)-np.nanmin(Pf_1stTrig))]
##        CDF_of_median=
#        Sigmas_std=   optimize.brent(f,brack=(0,10,20))
        #
        # \todo: find Sigmas_std -> density for low pressure bins --> repeaters
        #
