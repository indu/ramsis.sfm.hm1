#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 12:53:43 2019

@author: dkarvoun
"""
import numpy as np
from scipy.interpolate import CloughTocher2DInterpolator
from scipy.special import erf

import pandas as pd
import json
from obspy import read_events
import pymap3d as pm
from datetime import datetime
from scipy import stats, special
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
from sklearn import linear_model
import os
import matplotlib.colors as colors

def ExtractWellLogs_Numpy(PathOfFiles, string2time='%Y-%m-%dT%H:%M:%S', columns=['time', 'flow', 'pressure']):
    ListOfFiles=os.listdir(PathOfFiles)
    All_Logs=pd.DataFrame(columns=columns)
    for WellLogs2Read in ListOfFiles:
        logs_to_read = os.path.join(PathOfFiles, WellLogs2Read)
        print("Reading json file: ", logs_to_read)
        with open(logs_to_read) as f:
            well_data = json.load(f)
        sections_data = well_data['sections']
        hydraulics_data = pd.DataFrame(sections_data[0]['hydraulics'])
#        INJ_long, INJ_lat, INJ_depth=SectionLogs['bottomlongitude']['value'], SectionLogs['bottomlatitude']['value'], SectionLogs['topdepth']['value']
        INJ_start_time=sections_data[0]['starttime']
        t0=datetime.strptime(INJ_start_time,string2time).timestamp()
        Qinj_logs=np.array(pd.io.json.json_normalize(np.array(hydraulics_data['topflow'])))#-np.array(pd.io.json.json_normalize(np.array(hydraulics_data['bottomflow'])))
        print('Remove next two lines, which are added for hard coding the BS1 well logs')
        Qinj_logs2=np.array(pd.io.json.json_normalize(np.array(hydraulics_data['bottomflow'])))#-np.array(pd.io.json.json_normalize(np.array(hydraulics_data['bottomflow'])))
        Qinj_logs[np.where(Qinj_logs==0.)]=-Qinj_logs2[np.where(Qinj_logs==0.)]

        PCHS_logs=np.array(pd.io.json.json_normalize(np.array(hydraulics_data['toppressure'])))
        t_logs=np.array(pd.io.json.json_normalize(np.array(hydraulics_data.datetime)))
        for e in range(len(t_logs)):
            t_logs[e]=datetime.strptime(t_logs[e][0],string2time).timestamp()-t0
        LoadedWellLogs=pd.DataFrame.from_records(data=np.array([t_logs, Qinj_logs, PCHS_logs]).transpose()[0], columns=columns, coerce_float=True).sort_index()
        All_Logs=All_Logs.append(LoadedWellLogs)
    return All_Logs, sections_data


def CoarsenWellLogs_Panda(HydraulicLogs_raw, columns=['time', 'flow', 'pressure'], mean_dt_coarse=60.):
# Extract numpy's
    t_fine=np.array(HydraulicLogs_raw[columns[0]])

    median_dt_raw=np.median( np.diff(t_fine) )
    my_kernel_size=int( round(mean_dt_coarse/median_dt_raw) )
    my_cutoff_end=int( round( len(t_fine)%my_kernel_size ) )
    t_coarse=t_fine[:-my_cutoff_end].reshape(-1,my_kernel_size)
    Time_column   = np.append( t_coarse[:,0], t_fine[-1] )

    P_fine=np.array(HydraulicLogs_raw[columns[2]])
    P_coarse=P_fine[:-my_cutoff_end].reshape(-1,my_kernel_size)
    P_column      = np.append( np.median(P_coarse, axis=1), np.median( P_fine[-my_cutoff_end:] ) )

    error_P_column= np.sqrt( np.append( np.var(P_coarse, axis=1), np.var( P_fine[-my_cutoff_end:] ) ) ) 

    Q_fine=np.array(HydraulicLogs_raw[columns[1]])    
    Qcumulative=np.cumsum(Q_fine*np.diff(np.append(0., t_fine)))
    Q_coarse=Qcumulative[:-my_cutoff_end].reshape(-1,my_kernel_size)
    SumQ_column=np.append(Q_coarse[:,0], Qcumulative[-1])
    Q_column=np.append( 0., np.diff(SumQ_column)/np.diff(Time_column) )

    Coarse_Well_Logs = pd.DataFrame({columns[0]:Time_column, columns[1]: Q_column, columns[2]: P_column, 'P_error':error_P_column}).sort_index()
    return Coarse_Well_Logs



def Collect_QML_Catalog_to_pd (Catalog2Read,INJ_lat,INJ_long, INJ_depth,t_ZERO):
    obspy_catalog = read_events(Catalog2Read)
    dttime_column, mag_column, lat, lon, Z = zip(*[(e.preferred_origin().time.datetime.timestamp(),
                                           e.preferred_magnitude().mag,
                                           e.preferred_origin().latitude,
                                           e.preferred_origin().longitude,
                                           e.preferred_origin().depth)
                                        for e in obspy_catalog.events])
    Xs,Ys,Zs = pm.geodetic2enu(lat,lon,Z, INJ_lat,INJ_long, INJ_depth)
    ts=np.array(dttime_column)-t_ZERO.timestamp()
    catalog_incomplete_unbounded_df = (pd.DataFrame({'ts':ts, 'Xs':Xs, 'Ys':Ys, 'Zs':Zs, 'Mw': mag_column}).sort_index()).sort_values(by=['ts'],ascending=True)
    return catalog_incomplete_unbounded_df


    
def Mw_to_Mo(Mw):
    return np.power(10.,np.array((Mw+6.03)*1.5))

def Mw_to_Radius(Mw, StressDrop=2.e6):
    M0=Mw_to_Mo(Mw)
    R=(7./(16.*StressDrop))*M0
    return np.power(R, (1. / 3.))

def Find_Which_Ransac(X,y):
    ransac = linear_model.RANSACRegressor(random_state=42)
    ransac.fit(X,y)
    Clustered=np.where(ransac.inlier_mask_)[0]
    Remaining=np.where(ransac.inlier_mask_==False)
    return Remaining, Clustered, ransac.predict(X.iloc[Clustered])


def Augment_DataFrames(HydraulicLogs_raw, catalog_incomplete_unbounded_df):
    
    # Augment Seismic Catalog
    CSH_distance=np.sqrt(np.square(catalog_incomplete_unbounded_df.Xs)+np.square(catalog_incomplete_unbounded_df.Ys)+np.square(catalog_incomplete_unbounded_df.Zs))
    M0=np.array(Mw_to_Mo(np.array(catalog_incomplete_unbounded_df.Mw)))
    SumM0=np.cumsum(M0)
    Radii=np.array(Mw_to_Radius(np.array(catalog_incomplete_unbounded_df.Mw) ) )
    As=np.square(Radii)*np.pi
    SumAs=np.cumsum(As)
    
    catalog_incomplete_unbounded_df.insert(5,  'Distance_from_casing', CSH_distance)
    catalog_incomplete_unbounded_df.insert(6,  'Seismic_Moment', M0)
    catalog_incomplete_unbounded_df.insert(7,  'Cum_Seismic_Moment', SumM0)
    catalog_incomplete_unbounded_df.insert(8,  'Equivalent_Disk_Radius', Radii)
    catalog_incomplete_unbounded_df.insert(9,  'Ruptured_Surface', As)
    catalog_incomplete_unbounded_df.insert(10, 'Cum_Rupt_Surface', SumAs)

    # Augment Hydraulic Logs    
    InjectedPower =np.array(HydraulicLogs_raw['flow'])*np.array(HydraulicLogs_raw['pressure'])
    InjectedEnergy=np.cumsum(InjectedPower[1:]*np.diff(HydraulicLogs_raw['time']))
    HydraulicLogs_raw['Hydraulic_Power']=InjectedPower
    ts0=np.array(HydraulicLogs_raw.time)
    Size=len(ts0)-1
    SeismicWellLog=np.zeros(Size)
    for k in range(Size):
        SeismicWellLog[k]=SumM0[np.argmax(catalog_incomplete_unbounded_df.ts.values>ts0[k])]
    
    EnergiesRatio=(InjectedEnergy*1.e6)/SeismicWellLog
    EnergiesRatio[np.where(SeismicWellLog<1.)]=0.
    HydraulicLogs_raw.insert(4, 'Cum_Seismic_Energy', np.append(0.,SeismicWellLog))
    HydraulicLogs_raw.insert(5, 'Ratio_Seismic_Hydraulic_Energy', np.append(0.,EnergiesRatio) )
    HydraulicLogs_raw.plot(x='time', subplots=True)
    catalog_incomplete_unbounded_df.plot(x='ts', subplots=True)
    return HydraulicLogs_raw, catalog_incomplete_unbounded_df




def Process_All_RAMSIS_Observations(Catalog2Read ='cat-basel2006.qml', PATH_2_FIND_JSON='basel_json_files', MyTimeConversion='%Y-%m-%dT%H:%M:%S',MyColumns=['time', 'flow', 'pressure']):
    
    HydraulicLogs_raw, SectionLogs=ExtractWellLogs_Numpy(PATH_2_FIND_JSON, string2time=MyTimeConversion, columns=MyColumns)
    HydraulicLogs_raw=HydraulicLogs_raw.sort_values(by=['time'])  
    
    INJ_long, INJ_lat, INJ_depth=SectionLogs[0]['toplongitude']['value'], SectionLogs[0]['toplatitude']['value'], SectionLogs[0]['topdepth']['value']
    INJ_long_bottom, INJ_lat_bottom, INJ_depth_bottom=SectionLogs[0]['bottomlongitude']['value'], SectionLogs[0]['bottomlatitude']['value'], SectionLogs[0]['bottomdepth']['value']
    INJ_start_time=SectionLogs[0]['starttime']
    t_ZERO=datetime.strptime(INJ_start_time,MyTimeConversion)
    WGS84_CSH=[INJ_lat, INJ_long, INJ_depth]
    X0well=np.zeros(3)
    X1well= np.array(pm.geodetic2enu(INJ_lat_bottom,INJ_long_bottom, INJ_depth_bottom, WGS84_CSH[0], WGS84_CSH[1], WGS84_CSH[2]))    
    
    catalog_incomplete_unbounded_df = Collect_QML_Catalog_to_pd(Catalog2Read, WGS84_CSH[0], WGS84_CSH[1], WGS84_CSH[2], t_ZERO)
    
    return X0well, X1well, HydraulicLogs_raw, catalog_incomplete_unbounded_df, t_ZERO.timestamp()


def Collect_From_RAMSIS_Forecasting_Bins():
    """
     Temporary creation of forecasting bins and forecasting moments;
     RAMSIS is expected in the future to choose not only the spatial bins
     for the calibration/weighting, but also for which exact time-moments
     a forecast is asked. 
    """
    BinnedSpaceStart=np.array([       0.     , -1000., -1000., -1000.])
    BinnedSpaceEnd  =np.array([ 14.*24.*3600.,  1000.,  1000.,  1000.])
    NumberOfBins    =np.array([   10*4       ,  10   ,  10   ,  10])    
    ForTime_bins=np.linspace(BinnedSpaceStart[0], BinnedSpaceEnd[0], num=NumberOfBins[0]+1)
    ForX_bins   =np.linspace(BinnedSpaceStart[1], BinnedSpaceEnd[1], num=NumberOfBins[1]+1)
    ForY_bins   =np.linspace(BinnedSpaceStart[2], BinnedSpaceEnd[2], num=NumberOfBins[2]+1)
    ForZ_bins   =np.linspace(BinnedSpaceStart[3], BinnedSpaceEnd[3], num=NumberOfBins[3]+1)

#  Necessary Input for Forecast
    Forecasts_Edges=[ForX_bins, ForY_bins, ForZ_bins]
    Forecasts_Moments=ForTime_bins
    return Forecasts_Edges, Forecasts_Moments

def Restrict_Catalog_To_Forecasting_Bounds(c_df, Forecasts_Edges, Clm=['Xs', 'Ys', 'Zs']):
    m=np.amin(Forecasts_Edges, axis=1)
    M=np.amax(Forecasts_Edges, axis=1)
    print ("bounding catalog from ", m," until ", M)
    r_df=c_df.copy()
    for i in range (len(m)):
        r_df=r_df[ r_df[ Clm[i] ] > m[i] ]
        r_df=r_df[ r_df[ Clm[i] ] < M[i] ]
    return r_df



#%% Geldinganes
def HighRateCyclicPulse(fz, t_duration):
    SemiCycle=np.arange(t_duration*fz)
    dt=(SemiCycle+1)/fz
    if (dt[-1]<t_duration):
        dt.append(dt, t_duration)
    dQ=np.zeros(len(dt))
    Pulses=np.where(SemiCycle%2==0)
    dQ[Pulses]=1.
    return dt, dQ

def WellOperation_Pulse(t_start, t_end, Qmin, Qmax, fz=(1./60.)):
    Dt, DQ=HighRateCyclicPulse(fz, (t_end-t_start))
    Dt=Dt+t_start
    DQ=DQ*(Qmax-Qmin)+Qmin
    return Dt, DQ


#%% HM3

def Save_HM3_Planned_Inject(FilePlan, Strategy):
    HM4_Header='time      InjectedVolumeRate     Well\n0	0	0'
    np.savetxt(FilePlan, Strategy,fmt=['%f', '%f', '%i'], header=HM4_Header, delimiter=" ", newline="\n")#), footer='\n')
    return

def Save_HM3_Geldinganes_Planned_Injection(FileName='Stimulation design V4_basic.xlsx', Period_between_stages=(3.*24.-1.), Q_during_stages=0., Number_of_stages=3, my_sheet_name='Summary'):
    Planned_Stimulation_pd=pd.read_excel(FileName,  sheet_name=my_sheet_name)
    OperationsTimeStep=np.zeros(1)
    OperationsFlowRate=np.zeros(1)
    t_start, t_end= 0., 0.
    print(Planned_Stimulation_pd)
    for oper in range(len(Planned_Stimulation_pd)):
#        print(oper)
        my_operation=Planned_Stimulation_pd['Operation'][oper].split(",")
        t_start, t_end=Planned_Stimulation_pd['Start time (hr)'][oper]*3600., Planned_Stimulation_pd['End time (hr)'][oper]*3600.
        if(my_operation[0]=='Pulse'):
            Qmax, Qmin, fz=float(my_operation[1])*1.e-3,float(my_operation[2])*1.e-3,(1./float(my_operation[3]))
            DT, DQ=WellOperation_Pulse(t_start, t_end, Qmin, Qmax, fz)
            OperationsTimeStep=np.append(OperationsTimeStep, DT)
            OperationsFlowRate=np.append(OperationsFlowRate, DQ)
        elif(my_operation[0]=="Flowback"):
            OperationsTimeStep=np.append(OperationsTimeStep, t_end)
            OperationsFlowRate=np.append(OperationsFlowRate, -9999)
        else:
            OperationsTimeStep=np.append(OperationsTimeStep, t_end)
            OperationsFlowRate=np.append(OperationsFlowRate, Planned_Stimulation_pd['Flow rate (l/s)'][oper]*1.e-3)
    t_end=t_end+Period_between_stages*3600.
    OperationsTimeStep=np.append(OperationsTimeStep, t_end)
    OperationsFlowRate=np.append(OperationsFlowRate, Q_during_stages)
    Strategy=np.array([OperationsTimeStep, OperationsFlowRate, np.zeros(len(OperationsTimeStep))])
    for i in range(Number_of_stages-1):
        Strategy=np.append(Strategy,np.array([OperationsTimeStep+t_end*(1+i), OperationsFlowRate, np.zeros(len(OperationsTimeStep))+(i+1)]), axis=1)

    Save_HM3_Planned_Inject('Geldinganes_Planned_Injection', Strategy.transpose())
    return  Strategy.transpose()


def Calibrate_Observed_GR_Law_EM1_like(Incomplete_Catalog_df, dM=0.1, mwkey='Mw'):    
    """
     Very similar to ramsis.EM1 mc_mode
     The Frequency Mass Density (FMD) is found by smoothing
     observed Magnitudes (M) with Gaussian kernels. Then 
     the competion Magnitude (Mc) is set to the rounded value 
     at the peak of the FMD. 
     
     The slope of the Gutenberg-Richter law is returned with 
     the MLE method, Aki (1965) and for the found Mc.
     
     NOTE: It returns Nan when less than 5 events are passed.
      The scientific implications behind it need to be further
      studied (the same applies for the ramsis.EM1). E.g. binomial
      is more possible; here the mode with the largest M is chosen.
     
     :param Magnitudes: magnitudes of incomplete catalog
     :param dM        : size of binning and of rounding Mc
     
     :rtype: float for the Mc
     :rtype: float for the b-value     
     """
    print("starting the GR")
    Magnitudes=np.array(Incomplete_Catalog_df[mwkey])
    Mc, b =np.full(2, np.nan)
    print("inputs to gr: ", Incomplete_Catalog_df)
    print("len magnitudes: ", len(Magnitudes)>4)
    if(len(Magnitudes)>4):
        print("log dm", dM)
        Mrange=np.around(np.arange( np.amax(Magnitudes)+5*dM,np.amin(Magnitudes)-5*dM, -dM), decimals=int(-np.log(dM)))
        print("Mrange", Mrange)
        Mc=Mrange[np.argmax(stats.gaussian_kde(Magnitudes)(Mrange))]
        print("mc", Mc)
        b=np.log10(np.exp(1.))/(np.mean(Magnitudes[np.where(Magnitudes>=Mc)])-Mc)        
        print('b', b)
    return Mc, b

def Plot_First_GUI_Tab(catalog_incomplete_df, CurrentTime):
    Xs=catalog_incomplete_df.Xs
    Ys=catalog_incomplete_df.Ys
    Zs=catalog_incomplete_df.Zs
    mag_column=catalog_incomplete_df.Mw
    ts=catalog_incomplete_df.ts

    Mc, b_value =  Calibrate_Observed_GR_Law_EM1_like(catalog_incomplete_df, 0.005, 'Mw')
    catalog_complete_df=catalog_incomplete_df[catalog_incomplete_df['Mw']>Mc]
    a_value=b_value*Mc+np.log10(len(catalog_complete_df))

# prepare first subplot by finding the vertical plane on which to project the second subplot
    ransac = linear_model.RANSACRegressor(random_state=42)
    ransac.fit(np.array(Xs).reshape(-1,1),np.array(Ys).reshape(-1,1))
    fig =plt.figure(30, figsize=(40, 15))
    plt.rc('xtick',labelsize=18)
    plt.rc('ytick',labelsize=18)
    fig.suptitle('Observed Seismicity up until '+str(datetime.fromtimestamp(max(catalog_incomplete_df.ts)+CurrentTime))+' centered at the Casing Shoe',size=32)

    plt.subplot(1,3,1)
    plt.scatter(Xs, Ys, s=(np.power(mag_column,3)+1.)*10., marker='o', c=(ts/3600.), cmap=plt.cm.hsv, norm=colors.LogNorm())
    plt.plot(Xs,ransac.predict(np.array(Xs).reshape(-1,1)), linestyle='--', lw=3)
    plt.xlabel('Easting [m]',size=24)
    plt.ylabel('Northing [m]',size=24)
    plt.grid(True)
    plt.title('Map view',size=24)

# prepare second subplot by finding the projection on the chosen vertical plane
    SouthEast_vec=ransac.predict(np.ones(1).reshape(-1,1))-ransac.predict(np.zeros(1).reshape(-1,1))
    SouthEast_vec=np.array([1.,SouthEast_vec])/(np.sqrt(1+SouthEast_vec*SouthEast_vec))
    Xproj=np.array([Xs,Ys]).transpose().dot(SouthEast_vec.transpose())

    plt.subplot(1,3,2)
    plt.scatter(Xproj, Zs, s=(np.power(mag_column,3)+1.)*10., marker='o', c=(ts/3600.), cmap=plt.cm.hsv, norm=colors.LogNorm())
    plt.xlabel('South-Easting [m]', fontsize=24)
    plt.ylabel('depth [m]',size=24)
    plt.title('RANSAC least squares view',size=24)
    plt.colorbar().set_label('time [hours]',size=24)
    plt.grid(True)

    edges=np.arange(np.floor(min(catalog_incomplete_df['Mw'])*10.-1.25)/10, np.ceil(max(catalog_incomplete_df['Mw'])*10.-1.25)/10, 0.25)

    plt.subplot(1,3,3)
    plt.hist(catalog_incomplete_df['Mw'], bins=edges)
    plt.plot(edges, np.power(10.,a_value-edges*b_value), label='Gutenberg-Richter fit:\na='+str(a_value)+'\nb='+str(b_value)+'\nMc='+str(Mc))
    plt.yscale('log', nonposy='clip')
    plt.xlabel('Magnitude Mw',size=24)
    plt.ylabel('# of occurences',size=24)
    plt.title('histogram of located seismicity',size=24)
    plt.grid(True)
    plt.legend(loc='upper right',fontsize='xx-large')
    plt.savefig('Catalog_Collected.png')
    plt.show()
    plt.close(fig)

    
    return catalog_complete_df

#%%
    
def Plot_First_GUI_Tab_bottomRow(catalog_incomplete_df, CurrentTime, HydraulicLogs, PlannedInjection):
    myscale=[1./(3600.), 1.e3]
    plt.figure(40, figsize=(40, 15))
    plt.rc('xtick',labelsize=18)
    plt.rc('ytick',labelsize=18)
    Qs=np.where(CurrentTime>=PlannedInjection[:,0])
    ts=PlannedInjection[Qs,0]*myscale[0]
    Qs=PlannedInjection[Qs,1]*myscale[1]
    
    plt.subplot(1,2,1)
    plt.plot(HydraulicLogs['time']*myscale[0], HydraulicLogs['flow']*myscale[1],'k',ts, Qs, 'r' , lw=3)
    plt.axvline(x=CurrentTime)
    plt.xlabel('time [h]',size=24)
    plt.ylabel('Injection [l/s]',size=24)
    plt.grid(True)
    plt.title('Past and Planned Injection',size=24)   
    
    plt.subplot(1,2,2)
    plt.hist(catalog_incomplete_df.ts*myscale[0], histtype='step',cumulative=True, lw=3)
    plt.xlabel('time [h]',size=24)
    plt.ylabel('# of events',size=24)
    plt.grid(True)
    plt.savefig('Catalog_Collected_b.png')
    plt.show()
    plt.close()
    return

#%%
class Eszter_LL_score:
    def __init__(self, X , bins=10):
        self.y, self.edges=np.histogramdd(X,bins=bins, density=False)
        self.y=np.cumsum(self.y, axis=0)
        self.y=self.y.ravel()
        self.logyfact=np.log10(special.factorial(self.y)).ravel()

    def score(self, y1):
        LL=self.y*np.log10(y1.ravel())-y1.ravel()-self.logyfact
        return np.sum(LL)
    

#%%
