"""
HM1 model code that uses hydraulic borehole data and a seismicity catalog
to forecast the number of events above a certain magnitude based on a
hydraulics plan.
"""
import sys
import subprocess
import os
from shutil import copyfile
import pandas as pd
import logging
import argparse
import numpy as np
import random
import pymap3d as pm
from datetime import datetime, timedelta
from scipy.optimize import differential_evolution
import copy
import ramsis.sfm.hm1.core.KD_CAPS as CAPS
from ramsis.sfm.hm1.core.my_DK_RAMSIS import (
    Eszter_LL_score, Calibrate_Observed_GR_Law_EM1_like)
from ramsis.sfm.hm1.core.create_csv_input import CSVInputMixin
import time


BASE_EXTERNAL_DIR = os.path.join(os.path.dirname(
    os.path.realpath(__file__)), 'External_dir')

LOGGER = 'ramsis.sfm.hm1_model'
NAME = 'HM1MODEL'
logger = logging.getLogger(LOGGER)


class HM1(CSVInputMixin):
    def __init__(self,
                 seed_settings,
                 hm1_settings,
                 caps_settings,
                 time_reference,
                 training_epoch_duration,
                 mc_bin_size,
                 max_iterations,
                 differential_evolution_workers,
                 well_section_top,
                 well_section_bottom,
                 hm1_base_dir,
                 external_call_rel_path,
                 well_radius=None,
                 start_time=0,
                 recenter=None,
                 test_mode=False
                 ):
        print(" start HM1 external solution settings", hm1_settings)
        self.test_mode = test_mode
        self.datetime_format = '%d.%m.%Y %H:%M:%S'
        self.time_reference = time_reference
        self.training_epoch_duration = training_epoch_duration
        self.start_time = start_time
        self.recenter = recenter
        self.hm1_base_dir =   os.path.join(os.path.dirname(
            os.path.realpath(__file__)),hm1_base_dir)
        self.external_call = os.path.join(
            self.hm1_base_dir, external_call_rel_path)
        self.flow_col_name = 'flow'
        self.pressure_col_name = 'pressure'
        self.time_col_name = 'ts'
        self.spatial_cols = ['easting', 'northing', 'up']
        self.Mw_col_name = 'Mw'
        self.seed_settings = seed_settings
        #print("fluid density", self.seed_settings['fluid_density'])
        #for key in ['Lw1', 'Lw2', 'a', 'm', 'Mag_Completeness']:
        #    if not self.seed_settings[key]:
        #        self.settings[key] = None
        self.hm1_settings = hm1_settings
        self.caps_settings = caps_settings
        self.well_section_top = well_section_top
        self.well_section_bottom = well_section_bottom

        if well_radius:
            self.hm1_settings['well_radius'] = well_radius
        # Need to update this once we have decided where we want Dieter's code
        # to run.
        tmp_external_dir = "{}_{}".format(datetime.now().
                                          strftime('%Y%m%d%H%M'),
                                          random.randint(1000, 9999))
        self.external_dir = os.path.join(BASE_EXTERNAL_DIR, tmp_external_dir)
        self.tecplot_dir = os.path.join(self.external_dir, 'Tecplot')
        self.csv_dir = os.path.join(self.external_dir, 'CSV')
        self.data_dir = os.path.join(self.external_dir, 'Data')
        self.tmp_dir = os.path.join(self.external_dir, 'Tmp')

        # Not sure what the DataExchange dir is used for, assume that it
        #is only used in Dieter's code and we can place it where we want.
        self.data_exchange_dir = os.path.join(self.external_dir,
                                              'DataExchange')
        # Output from Dieter's code
        self.pressure_forecast_filepath = os.path.join(
            self.tecplot_dir, 'Press_Dist_Forecast.dat')
        self.pressure_calibration_filepath = os.path.join(
            self.tecplot_dir, 'Press_Dist_Calib.dat')
        self.transmissivity_calculated_filepath = os.path.join(
            self.tecplot_dir, 'Transmissivities_calculated.dat')
        self.storage_calculated_filepath = os.path.join(
            self.tecplot_dir, 'StorageCoefficients_calculated.dat')
        self.Read_HM1_Cloud = os.path.join(self.csv_dir, 'CloudPara.csv')
        # Input to Dieter's code
        self.hydraulics_pathname = os.path.join(
            self.data_dir, 'Q_Measured_Calibrate.SED_CSV.csv')
        self.hydraulics_columns = ['Time(UTC)', 'Q(l/min)']
        self.well_pressure_pathname = os.path.join(
            self.data_dir, 'P_Measured.SED_CSV.csv')
        self.planned_hydraulics_pathname = os.path.join(
            self.data_dir, 'Q_Measured_Forecast.SED_CSV.csv')

        self.well_pressure_columns = ['Time(UTC)', 'p(Pa)']
        self.catalog_pathname = os.path.join(
            self.data_dir, 'SeismicCatalog_Measured.SED_CSV.csv')
        self.catalog_columns = ['Time',
                                'Offset-X(m)',
                                'Offset-Y(m)',
                                'Offset-Z(m)',
                                'Local magnitude']
        # Input config files for Dieter's code
        self.parameter_dir = os.path.join(
            self.external_dir, 'Parameters')
        calibrate_csv = 'input_para_CalibratePress.csv'
        forecast_csv = 'input_para_ForecastPress.csv'
        self.input_para_calibrate_press = os.path.join(
            self.parameter_dir, calibrate_csv)
        self.input_para_forecast_press = os.path.join(
            self.parameter_dir, forecast_csv)
        self.system_config = os.path.join(self.parameter_dir, 'system_config.csv')
        if self.test_mode:
            self.calibration_command = [
                'echo', 'Running test calibration command']
            self.forecast_command = [
                'echo', 'Running test forecast command']
        else:
            self.calibration_command=[
                self.external_call, '--Operation',
                'CalibratePress', '--InputParaFile',
                self.input_para_calibrate_press]
            self.forecast_command=[
                self.external_call, '--Operation',
                'ForecastPress', '--InputParaFile',
                self.input_para_forecast_press]

        self.input_para_common = os.path.join(
            self.parameter_dir, 'input_para_common_01.csv')
        # Initialize attributes to store data with corrected names and units
        # for Dieter's required inputs
        self.training_hydraulics = None
        self.training_catalog = None

        self.cost_function = None
        self.ll_edges = None
        self.ll_moments = None
        self.training_max_pressure = None
        self.training_xyz = None
        self.training_volume = None

        self.optimize_keys = ["stressdrop_coeff",
                              "PoissonDensity",
                              "sigma1_std",
                              "min_failure_pressure"]
        # What are these values and the structure they are in?
        # Can any more be moved to settings file, or are they constant?
        self.bounds = [(seed_settings["stressdrop_coeff"] * 0.5,
                       seed_settings["stressdrop_coeff"] * 1.5),
                       (-10, 10), # Poisson density
                       (0., seed_settings["sigma1_std"]),
                       (1.e-2, seed_settings["min_failure_pressure"])]

        self.logscale = [False, True, False, False]
        self.disp = True
        self.maxiter = max_iterations
        self.differential_evolution_workers = differential_evolution_workers
        self.seed = 42
        # I don't see where this attribute is used?
        self.mc_bin_size = mc_bin_size
        # Conversion from cubic metres per second to litres per min
        self.flow_conversion = 1.0e3 * 60.0
        self.make_new_directories()

    def make_new_directories(self):
        for pathname in [self.external_dir,
                         self.tecplot_dir,
                         self.csv_dir,
                         self.data_dir,
                         self.parameter_dir,
                         self.tmp_dir,
			 self.data_exchange_dir]:
            if not os.path.exists(pathname):
                os.makedirs(pathname)

    def update_seeds_settings(self):
        # would you rather the Mc was calculated or gained
        # from the QuakeMl data?
        Mc, b_value = Calibrate_Observed_GR_Law_EM1_like(
            self.training_catalog, self.mc_bin_size, self.Mw_col_name)
        self.seed_settings["Mag_Completeness"] = Mc
        self.seed_settings["b_value"] = b_value
        self.training_catalog=self.training_catalog[
            self.training_catalog[self.Mw_col_name]>=Mc]

        if (self.seed_settings["b_vs_depth_range"]):
            a, m = CAPS.b_value_Lasso_calibration(
                np.array(self.training_catalog[self.spatial_cols].values),
                np.array(self.training_catalog[[self.Mw_col_name]])[:, 0],
                bins=100)
        else:
            a, m = b_value, np.zeros(3)

        self.seed_settings["a"] = a
        self.seed_settings["m"] = m

    def update_hm1_settings(self):

        print("update hm1_settings external solution settings", self.hm1_settings)
        # store the start settings somewhere new as hm1_settings
        # will be updated
        self.seed_settings['Lw1'] = self.well_section_top
        self.seed_settings['Lw2'] = self.well_section_bottom
        print("setting Lw1: ", self.well_section_top)
        print("setting Lw2: ", self.well_section_bottom)
        self.seed_settings['L'] = [
            abs(coord1-coord2) for coord1, coord2 in
            zip(self.well_section_top, self.well_section_bottom)]
        self.hm1_settings['Lw1'] = self.well_section_top
        self.hm1_settings['Lw2'] = self.well_section_bottom
        XYZ=np.array(self.training_catalog[self.spatial_cols])
        #print("XYZ", XYZ[0:20])
        Cloud_Shape = pd.read_csv(self.Read_HM1_Cloud, sep=';', index_col=0)
        Center=[Cloud_Shape['Value']['x0'],
                Cloud_Shape['Value']['y0'],
                Cloud_Shape['Value']['z0']]

        NormalVec=[Cloud_Shape['Value']['n_x'],
                   Cloud_Shape['Value']['n_y'],
                   Cloud_Shape['Value']['n_z']]

        NormalVec = NormalVec / np.linalg.norm(NormalVec)
        widths = (XYZ - Center).dot(NormalVec)

        width = np.abs(np.percentile(widths, 95) - np.percentile(widths, 5))
        self.hm1_settings['Lw1'] = Center + NormalVec * (0.5 * width)
        self.hm1_settings['Lw2'] = Center - NormalVec * (0.5 * width)
        print("After external calibration")
        print("setting Lw1: ", self.hm1_settings['Lw1'])
        print("setting Lw2: ", self.hm1_settings['Lw2'])
        print("after update hm1_settings external solution settings", self.hm1_settings)

    def ll_score(self, x):
        y = self.cost_function.y
        log_factorial_y = self.cost_function.logyfact
        TestSettings = copy.deepcopy(self.seed_settings)
        for i in range(len(x)):
            TestSettings[self.optimize_keys[i]] = x[i]
            if (self.logscale[i]):
                TestSettings[self.optimize_keys[i]] = np.power(10., x[i])
        y1 = CAPS.Forecast_BinnedRate_only(
            self.ll_moments[1:], self.ll_edges, self.training_max_pressure,
            self.training_xyz, self.training_volume, TestSettings)
        y1 = y1.ravel()
        y1[np.where(y1 <= 0.)] = 1.e-20
        LL = np.nanmean(y * np.log10(y1) - y1 - log_factorial_y)
        #LL = np.sum(y * np.log10(y1) - y1 - log_factorial_y) / float(len(y1))
        return -LL

    def ls_score(self, x):
        y = self.cost_function.y
        TestSettings = copy.deepcopy(self.seed_settings)
        for i in range(len(x)):
            TestSettings[self.optimize_keys[i]] = x[i]
            if (self.logscale[i]):
                TestSettings[self.optimize_keys[i]] = np.power(10., x[i])
        y1 = CAPS.Forecast_BinnedRate_only(
            self.ll_moments[1:], self.ll_edges, self.training_max_pressure,
            self.training_xyz, self.training_volume, TestSettings)
        y2 = np.sum(y1,axis=(1,2,3))
        print(y2)
        ls=np.nanmean(np.square(y-y2))
        return ls

    def calibrate_caps(self,hm1_edges):
        print("calibrate caps")
        self.update_hm1_settings()
        self.update_seeds_settings()
        self.training_catalog[self.time_col_name] = (
            self.training_catalog.index - # noqa
            self.training_hydraulics.index.min()).total_seconds()
        print("ts not found...", self.training_catalog, self.spatial_cols)
        self.cost_function = Eszter_LL_score(
            np.array(self.training_catalog[
                np.append(self.time_col_name, self.spatial_cols)]), bins=5)

        self.ll_edges = self.cost_function.edges[1:4]
        self.ll_moments = self.cost_function.edges[0]-self.training_hydraulics.time.values[-1]
        print("Interpolate max pressure 1")
        print("before calling max pressure hm1_settings external solution settings", self.hm1_settings)

        (self.training_max_pressure,
         self.training_xyz,
         self.training_volume) = CAPS.Interpolate_Maximum_Pressure(
            self.ll_moments, self.ll_edges, self.hm1_settings,
            self.caps_settings, self.pressure_calibration_filepath)

        print("Maximum iterations:", self.maxiter)
        result = differential_evolution(
            self.ll_score, self.bounds, disp=self.disp,
            maxiter=self.maxiter, seed=self.seed, workers=self.differential_evolution_workers)

        for i in range(len(result.x)):
            if self.logscale[i]:
                self.seed_settings[
                    self.optimize_keys[i]] = np.power(10., result.x[i])
            else:
                self.seed_settings[self.optimize_keys[i]] = result.x[i]
        # correct Poisson Density for systematic errors mismatches due to HM1

        print("End differential evolution")

        # past seismicity with calibrated caps
        (max_pressure_past,
        working_xyz_past,
        cells_volume_past) = CAPS.Interpolate_Maximum_Pressure(
                    self.training_hydraulics.time.values[1:]-self.training_hydraulics.time.values[-1], hm1_edges, self.hm1_settings,
                    self.caps_settings, self.pressure_calibration_filepath)

        self.total_events_past = CAPS.Forecast_BinnedRate_only(
              self.training_hydraulics.time.values[1:]-self.training_hydraulics.time.values[-1],
              hm1_edges, max_pressure_past, working_xyz_past,
              cells_volume_past, self.seed_settings)

        print('Poisson Density CAPS:', self.seed_settings["PoissonDensity"])

        print("sum of total events after calibration", np.sum(self.total_events_past[-1, :, :, :]), )

#        NewPoisson = (self.seed_settings['PoissonDensity']
#                      * len(self.training_catalog.time) / (np.sum(self.total_events_past[-1, :, :, :])))  # noqa
        # If This NewPoisson value is inf, then should this be discarded
        # rather than being set in the settings?
        # added the if statement below...
#        if np.isfinite(NewPoisson):
#            self.seed_settings['PoissonDensity'] = NewPoisson

        My_PressureFunction, External_timesteps = CAPS.Interpolator_Of_External_Pressure_Solution_Ptx(
            self.pressure_calibration_filepath, columns=self.hm1_settings["FileCols"],
            UnitConversion=self.hm1_settings["FileConvs"], SkipRows=self.hm1_settings["SkipLines"])

        final_time = (External_timesteps[-1] +self.training_hydraulics.time.values[-1])
        len_cat = len(self.training_catalog[self.training_catalog.ts <= final_time])
        NewPoisson = (self.seed_settings['PoissonDensity']
                      * len_cat / (np.sum(self.total_events_past[-1, :, :, :])))  # noqa
        # If This NewPoisson value is inf, then should this be discarded
        # rather than being set in the settings?
        # added the if statement below...
        if np.isfinite(NewPoisson):
            self.seed_settings['PoissonDensity'] = NewPoisson

        print('Poisson Density CAPS:', self.seed_settings["PoissonDensity"])

        self.total_events_past = CAPS.Forecast_BinnedRate_only(
            self.training_hydraulics.time.values[1:] - self.training_hydraulics.time.values[-1],
            hm1_edges, max_pressure_past, working_xyz_past,
            cells_volume_past, self.seed_settings)

        print("sum of total events after Re-calibration", np.sum(self.total_events_past[-1, :, :, :]), )


        if External_timesteps[-1]<-1.e-3:
            NewPoisson = (self.seed_settings['PoissonDensity']
                          * len(self.training_catalog)/(np.sum(self.total_events_past[-1,:,:,:]))) # noqa
            # If This NewPoisson value is inf, then should this be discarded
            # rather than being set in the settings?
            # added the if statement below...
            if np.isfinite(NewPoisson):
                self.seed_settings['PoissonDensity']=NewPoisson

        print('Poisson Density CAPS:', self.seed_settings["PoissonDensity"])


    def save_hm1_well_logs(self):
        self.training_hydraulics[self.flow_col_name] *= self.flow_conversion

        self.training_hydraulics[[
            self.flow_col_name,
            self.pressure_col_name]] = self.training_hydraulics[
                [self.flow_col_name, self.pressure_col_name]].fillna(0)
        training_hydraulics = self.training_hydraulics.copy(deep=True)
        training_hydraulics.rename(
            columns={'flow': self.hydraulics_columns[1],
                     'pressure': self.well_pressure_columns[1]},
            inplace=True)
        training_hydraulics.fillna(0.0, inplace=True)
        with open(self.hydraulics_pathname, 'w', newline='') as f:
            f.write(training_hydraulics.to_csv(
                sep=';', columns=[self.hydraulics_columns[1]],
                index_label=self.hydraulics_columns[0],
                date_format=self.datetime_format))

        with open(self.well_pressure_pathname, 'w', newline='') as f:
            f.write(training_hydraulics.to_csv(
                sep=';', columns=[self.well_pressure_columns[1]],
                index_label=self.well_pressure_columns[0],
                date_format=self.datetime_format))

    def save_hm1_catalog(self):
        with open(self.catalog_pathname, 'w', newline='') as f:
            training_catalog_cols = self.spatial_cols.copy()
            training_catalog_cols.append(self.Mw_col_name)
            # passing to HM1 the catalog by changing the first and last event time
            # to match the start/end time of hydraulics
            cat_to_file = self.training_catalog.copy(deep = True)
            #cat_to_file.index.values[0] = self.training_hydraulics.index.values[0]
            #cat_to_file.index.values[-1] = self.training_hydraulics.index.values[-1]
            f.write(cat_to_file.to_csv(sep=';',
                                       columns=training_catalog_cols,
                                       index_label=self.catalog_columns[0],
                                       header=self.catalog_columns[1:5],
                                       date_format=self.datetime_format))

    def fit(self, HydraulicLogs_coarse_pd, catalog_incomplete_df, hm1_edges, deep=True):
        self.training_hydraulics=HydraulicLogs_coarse_pd.copy(deep)
        self.save_hm1_well_logs()
        self.training_catalog = catalog_incomplete_df.copy(deep)
        # The recentering is now done in the model adaptor, so I will leave
        # it in until sure that there is no
        if self.recenter:
            self.training_catalog[
                self.spatial_cols] = (self.training_catalog[self.xs]
                                      + self.recenter) # noqa
        self.save_hm1_catalog()
        # Create csv files for external code
        self.create_input_para_calibrate_press()
        self.create_input_para_common()
        self.create_system_config_csv()
        # Change to subprocess command, and output result depending on
        # command success.
        os.chdir(self.hm1_base_dir)
        start_time = time.time()
        retval = subprocess.run(self.calibration_command, shell=True)
        end_time = time.time()
        print("Elapsed time to run HM1 was %g seconds" % (end_time - start_time))
        #print(retval)
        if retval.returncode != 0:
            raise IOError("HM1 error when calling calibration command with "
                          "subprocess. return code: "
                          f"{retval.returncode}")
        # Test mode will make sure Dieter's results are copied rather
        # than code called.
        if self.test_mode:
            #if self.well_section_top[2] > 4000:
            #    # The postman and integration tests use different
            #    # data, so must alter the depth of output data.
            #    existing_cloudpara = os.path.join(
            #        os.path.dirname(os.path.realpath(__file__)), 'CSV',
            #        'CloudPara_integration.csv')
            #else:
            existing_cloudpara = os.path.join(
                    os.path.dirname(os.path.realpath(__file__)), 'CSV',
                    'CloudPara.csv')
            copyfile(existing_cloudpara, self.Read_HM1_Cloud)
            existing_press_dist = os.path.join(
                os.path.dirname(os.path.realpath(__file__)), 'Tecplot',
                'Press_Dist_Calib.dat')
            copyfile(existing_press_dist, self.pressure_calibration_filepath)
            existing_press_dist2 = os.path.join(os.path.dirname(
                os.path.realpath(__file__)), 'Tecplot',
                'Press_Dist_Forecast.dat')
            copyfile(existing_press_dist2, self.pressure_forecast_filepath)
        # End test mode changes.

        self.calibrate_caps(hm1_edges)

        self.hm1_settings["FileNameSolutions"] = (
            self.pressure_forecast_filepath)

        f = open(os.path.join(os.path.dirname(os.path.realpath(__file__)),"Calibrated_CAPS.txt"), "w")
        f.write(str(self.seed_settings))
        f.close()

    def recenter_edges(self, forecast_edges):
        # I think that we should not recenter the edges as results
        # should always be returned for the reservoir area requested.
        my_edges=np.copy(forecast_edges)
        my_edges[0]+=self.recenter[0]
        my_edges[1]+=self.recenter[1]
        my_edges[2]+=self.recenter[2]
        return my_edges

    def save_hm1_forecast_injection(self, planned_strategy, epoch_duration):
        planned_strategy[self.flow_col_name] *= self.flow_conversion

        offset_string = f"{epoch_duration}S"
        new_hydraulic_data = resample_dataframe(planned_strategy,
                                                resample_str=offset_string,
                                                function='pad')
        new_hydraulic_data.rename(columns={'flow': self.hydraulics_columns[1]},
                                  inplace=True)
        #print("data from hydraulics...", new_hydraulic_data)
        new_hydraulic_data.fillna(0.0, inplace=True)

        new_hydraulic_data[self.hydraulics_columns[1]] *= self.flow_conversion
        with open(self.planned_hydraulics_pathname, 'w', newline='') as ofile:
            ofile.write(new_hydraulic_data.to_csv(
                sep=';', columns=[self.hydraulics_columns[1]],
                index_label=self.hydraulics_columns[0],
                date_format=self.datetime_format))

    def predict(self, planned_strategy, Forecasts_Moments,
                hm1_edges, resample_frequency):
        # See method for detail why this is commented out
        #if self.recenter and hm1_edges:
        #    hm1_edges=self.recenter_edges(hm1_edges)
        (max_pressure_past,
         My_Working,
         cells_volume) = CAPS.Interpolate_Maximum_Pressure(
            Forecasts_Moments, hm1_edges, self.hm1_settings,
            self.caps_settings, self.pressure_calibration_filepath)
        self.save_hm1_forecast_injection(planned_strategy, resample_frequency)

        # if self.test_mode == False:
        #     # apparently the new parameters are not used, and some other stored value is taken for the forecast
        #     stor_file = np.loadtxt(open(self.storage_calculated_filepath, "rt"), delimiter=" ", skiprows=6)
        #     self.hm1_settings["initial_storage_coeff"] = np.median(stor_file[:, 1])
        #     self.hm1_settings["borehole_storage_coefficient"] = np.median(stor_file[:, 1])
        #
        #     tran_file = np.loadtxt(open(self.transmissivity_calculated_filepath, "rt"), delimiter=" ", skiprows=6)
        #     self.hm1_settings["initial_transmissivity"] = 10**np.median(np.log10(tran_file[tran_file[:,1]>0, 1]))
        #     self.hm1_settings["borehole_transmissivity"] = 10 ** np.median(np.log10(tran_file[tran_file[:, 1] > 0, 1]))
        #     print("new settings for forecast")
        #     print(self.hm1_settings)

        # Create csv files for external code
        self.training_epoch_duration = 600
        self.create_input_para_common()
        self.create_input_para_forecast_press()
        retval = subprocess.run(self.forecast_command, shell=True)
        print(retval)
        if retval.returncode != 0:
            raise IOError("HM1 error when calling forecast command with "
                          "subprocess. return code: "
                          f"{retval.returncode}")
        (max_pressure,
         working_xyz,
         cells_volume) = CAPS.Interpolate_Maximum_Pressure(
             Forecasts_Moments, hm1_edges, self.hm1_settings,
             self.caps_settings, self.pressure_forecast_filepath)

        max_pressure = np.maximum(max_pressure_past, max_pressure)

        #np.set_printoptions(threshold=sys.maxsize)
        print("Forecast_TotalEvents_Bvalue inputs: cells_volume", cells_volume,"self.seed_settings", self.seed_settings)
        #np.save("C:/Users/ramsis/Forecasts_Moments", Forecasts_Moments)
        #np.save("C:/Users/ramsis/hm1_edges", hm1_edges)
        #np.save("C:/Users/ramsis/max_pressure", max_pressure)
        #np.save("C:/Users/ramsis/working_xyz", working_xyz)

        (NormalizedBinnedSeismicity,
         total_events,
         total_max_mw) = CAPS.Forecast_TotalEvents_Bvalue(
             Forecasts_Moments, hm1_edges, max_pressure, working_xyz,
             cells_volume, self.seed_settings)

        print("sum of total events after forecast", np.sum(total_events[-1,:,:,:]), "Max_Mw", np.max(total_max_mw))

        return NormalizedBinnedSeismicity, total_events, total_max_mw


def resample_dataframe(dataframe, resample_str='min', function='pad'):
    """
    There is a good tutorial here explaining all the options:
    http://benalexkeen.com/resampling-time-series-data-with-pandas/
    Functions: backwards filling, interpolation are available.
    """
    # Forward filling, so that the gaps will contain the previous value.
    dataframe_resampler = dataframe.resample(resample_str)
    # Apply the function to the resampler as to how values should be populated.
    new_dataframe = getattr(dataframe_resampler, function)()
    return new_dataframe


def dataframe_add_seconds_column(df, starting_date, column_name='seconds'):
    df[column_name] = df.index.dt.total_seconds()
    return df


def hydraulic_local_coords(section):

    (top_long,
     top_lat,
     top_depth,
     bottom_long,
     bottom_lat,
     bottom_depth) = (section['bottomlongitude_value'],
                      section['bottomlatitude_value'],
                      section['bottomdepth_value'],
                      section['toplongitude_value'],
                      section['toplatitude_value'],
                      section['topdepth_value'])
    wellhead_enu = np.zeros(3)
    injection_enu = np.array(pm.geodetic2enu(bottom_lat,
                             bottom_long, bottom_depth,
                             top_lat, top_long, top_depth))
    return wellhead_enu, injection_enu

def calculate_gr_relation_a(b, mc, number_events, epoch_duration):
    """
    Calculate the Gutenberg Richter parameter 'a' based on the
    predicted number of events on the time scale of a year.
    :param b: float, G-R parameter b
    :param mc: float, magnitude of completeness of the catalog
    :param number_events: DataFrame with column
    :param epoch_duration: length of all forecasts in seconds

    :rtype: same as number_events
    """
    print("number_events max", np.max(number_events))
    a = b * mc + np.log10(number_events * 86400.0*365.0 / epoch_duration)
    print("a max: ", np.max(a))
    return a

def exec_model(catalog,
               hydraulics,
               hydraulics_plan,
               end_training,
               training_epoch_duration,
               training_magnitude_bin,
               training_events_threshold,
               datetime_start,
               datetime_end,
               epoch_duration,
               forecast_edges,
               max_iterations,
               differential_evolution_workers,
               seed_settings,
               external_solution_settings,
               caps_kd_settings,
               mc_bin_size,
               well_radius,
               well_section_top,
               well_section_bottom,
               test_mode,
               external_call,
               hm1_base_dir):
    """
    Run the model training and forecast stages.

    :param catalog: Catalog of seismicity containing all seismicity between
        the dates end_training and end_training - training_epoch_duration.
    :param: hydraulics: Hydraulic measurements of the volume flowing into
        the well between the dates of end_training and
        end_training - training_epoch_duration
    :param: hydraulics_plan: Hydraulic plan of the volume flowing into
        the well between the dates of datetime_start and
        datetime_end
    :param training_epoch_duration: Time to train parameters for.
    :param training_magnitude_bin: Binning width in dB for seismic activity.
    :param training_events_threshold: Number of events threshold to
        return values
    :param datetime_start: Start of period to forecast.
    :param datetime_end: End of period to forecast.
    :param epoch_duration: Number of seconds between returned forecast
        rate values.

    :rtypes: (a) float, (b) float, (mc) float, (forecast_values) pd.DataFrame
    """
    # Based on what Antonio added to the model - the hydraulics require
    # a new column which is the number of seconds since the first training sample
    # finding the difference between datetimes converts to nanoseconds
    hydraulics['time'] = [i/1e9 for i in (hydraulics.index.values - hydraulics.index.values[0]).tolist()]
    hydraulics_plan['time'] = [i/1e9 for i in (hydraulics_plan.index.values - hydraulics.index.values[0]).tolist()]
    #print("catalog", catalog)
    reference_time = (end_training - # noqa
                      timedelta(seconds=training_epoch_duration))
    hydraulics = resample_dataframe(hydraulics, resample_str='min',
                                    function='pad')

    hm1 = HM1(seed_settings,
              external_solution_settings,
              caps_kd_settings,
              reference_time,
              training_epoch_duration,
              mc_bin_size,
              max_iterations,
              differential_evolution_workers,
              well_section_top,
              well_section_bottom,
              hm1_base_dir,
              external_call,
              well_radius=well_radius,
              test_mode=test_mode)
    hm1.fit(hydraulics, catalog, forecast_edges)
    print("Completed calibration")
    logger.info("Completed calibration")
    forecast_seconds = (datetime_end - datetime_start).total_seconds()
    forecast_start = 0  # (sarsonl) Would this be time between end of
    # observations and start of forecast?
    # Add a second on as arrange will discard the final second.
    forecast_end = forecast_start + forecast_seconds + 1
    # If the forecast periods do not completely fit into the forecast time,
    # The remainder will get cut off.
    forecast_dates = np.arange(forecast_start,
                               forecast_end,
                               epoch_duration)
    forecast = hm1.predict(hydraulics_plan,
                           forecast_dates,
                           forecast_edges,
                           epoch_duration)
    print("Completed forecast")
    logger.info("Completed forecast")
    binned_seismicity, forecast_output, total_max_mw = forecast
    mc = seed_settings["Mag_Completeness"]
    b = seed_settings["b_value"]
    print("forecast cumulative max", np.max(forecast_output))
    # The forecast is cumulative, find the difference between dates
    
    #forecast_incremental = np.zeros(forecast_output.shape)
    forecast_incremental = np.diff(forecast_output, axis=0)
    #forecast_incremental[0, :, :, :] = forecast_output[0, :, :, :]
    # The a value has a relationship to number of events
    forecast_incremental[forecast_incremental < 0.0] = 0.0
    
    print("forecast incremental max", np.max(forecast_incremental))
    #a = np.log10(forecast_incremental)

    a_scaled = calculate_gr_relation_a(b, mc, forecast_incremental, epoch_duration)
    ## Have chosen something that is a reasonable number of precision to get
    ## rid of -inf
    a_scaled[a_scaled<-1.0e15] = -1.0e15

    #np.save("C:/Users/ramsis/forecast_output", forecast_output)
    #np.save("C:/Users/ramsis/a_scaled", a_scaled)
    return forecast_dates, forecast_incremental, total_max_mw,  mc, b, a_scaled

def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%dT%H:%M:%s")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def parseargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('datetime_start', type=valid_date,
                        help='Start date for forecast, %Y-%m-%dT%H:%M:%s')
    parser.add_argument('datetime_end', type=valid_date,
                        help='End date for forecast, %Y-%m-%dT%H:%M:%s')
    parser.add_argument('epoch_duration', type=float,
                        help='Time between forecasts in seconds')
    parser.add_argument('training__epoch_duration', type=float,
                        help='Number of seconds to train for. If None, looks'
                        ' back over full seismic catalog')
    parser.add_argument('training_magnitude_bin', type=float,
                        help='Value in dB for binning seismic magnitudes')
    parser.add_argument('training_events_threshold', type=int,
                        help='number of events required within a forecast'
                        ' time window')
    parser.add_argument('catalog', type=list,
                        help='Catalog of seismic events from same'
                        ' period as hydraulic_input. List of tuples.'
                        ' e.g.[(date, {lat: 10.2, lon: 10.2,'
                        ' depth: 0, mag: 4.5})]')
    parser.add_argument('hydraulics', type=list,
                        help='Hydraulic sample data from the past, from same'
                        ' period as seismic catalog. list of tuples'
                        ' e.g. [(date, {flow_dh: 10.2, flow_xt: 10.2, '
                        'pressure_dh: 0, pressure_xt: 4.5})]')
    parser.add_argument('hydraulics_scenario',
                        help='Hydraulic sample data planned for the future.'
                        ' of same construct as hydraulic_input')
    parser.add_argument('end_training', type=valid_date,
                        help='End of training datetime.')
    parser.add_argument('max_iterations', type=int,
                        help='max iterations differential evolution.')
    parser.add_argument('differential_evolution_workers', type=int,
                        help='number workers differential evolution.')
    parser.add_argument('seed_settings', type=dict,
                        help='Settings for seed in HM1 model.')
    parser.add_argument('external_solution_settings', type=dict,
                        help='Settings in CAPS code for external code.')
    parser.add_argument('caps_kd_settings', type=float,
                        help='Settings for caps kd module.')
    parser.add_argument('well_radius', type=float,
                        help='Radius of well, casing width subtracted.')
    parser.add_argument('mc_bin_size', type=float,
                        help='Bin size used to divide magnitude into bins')

    parser.add_argument('well_section_top', type=list,
                        help='list of local coordinates for well section top'
                        ',reference point of well head')
    parser.add_argument('well_section_bottom', type=list,
                        help='list of local coordinates for well section '
                        'bottom, reference point of well head')
    parser.add_argument('test_mode', type=bool,
                        help='Play code in test mode. No external '
                        'code called, dummy data used.')
    parser.add_argument('external_call', type=str,
                        help='Relative path to external code for HM1'
                        ' from hm1_base_dir')
    parser.add_argument('hm1_base_dir', type=str,
                        help='Path to base dir of external code for HM1.')
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parseargs()
    results = exec_model(args.catalog,
                         args.hydraulics,
                         args.hydraulics_scenario,
                         args.end_training,
                         args.training_epoch_duration,
                         args.training_magnitude_bin,
                         args.training_events_threshold,
                         args.datetime_start,
                         args.datetime_end,
                         args.epoch_duration,
                         args.forecast_edges,
                         args.max_iterations,
                         args.differential_evolution_workers,
                         args.seed_settings,
                         args.external_solution_settings,
                         args.caps_kd_settings,
                         args.mc_bin_size,
                         args.well_radius,
                         args.well_section_top,
                         args.well_section_bottom,
                         args.test_mode,
                         args.external_call,
                         args.hm1_base_dir)
