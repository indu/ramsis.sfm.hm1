#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 13:31:04 2019

@author: dkarvoun
"""
import numpy as np
import my_DK_RAMSIS as DK
import Worker_HM1 as Dieter
import os
import json

mean_dt_coarse=60.
print("THE FORMAT OF TIME READ BY JSON NEEDS TO NOT DIFFER FROM %Y-%m-%dT%H:%M:%S -- Perhaps save format in json?")
print("Json files are my only source for start-time. If this is not the same globally, I cannot convert seismicity.")
print("Give a Maximum Distance (in the East-North-Up system) for filtering Catalog of Induced Seismicity")
print("We coarsen measurements for a new time step with median approx.", mean_dt_coarse, " seconds. Revisit append function when the json file starting from 0 is found")

print("Expect Forecasts_Edges for the calibration")

QML_RealTime_Catalog =os.path.join('RAMSIS_LAST_FETCH', 'qml_file', 'cat-basel2006.qml')
PATH_2_FIND_ONLY_JSON=os.path.join('RAMSIS_LAST_FETCH', 'json_file')
File=os.path.join('RAMSIS_LAST_FETCH', 'Planned_Injection', 'Stimulation_design_V4_basic.xlsx')
Output_Forecast='HM1_Forecast.csv'

MyJSONTimeConversion='%Y-%m-%dT%H:%M:%S'
MyColumns=['time', 'flow', 'pressure']

Forecasts_Edges, Forecasts_Moments = DK.Collect_From_RAMSIS_Forecasting_Bins()
print('Forecasts_Edges, Forecasts_Moments',Forecasts_Edges, Forecasts_Moments)

#%% Planned Activity
Hours_between_stages=(3.*24.-1.)
Q_during_stages=0.
Number_of_stages=3
t_now=4.5*3600.*24.
PlannedStrategy=DK.Save_HM3_Geldinganes_Planned_Injection(File, Hours_between_stages, Q_during_stages, Number_of_stages, 'Summary')
print('PlannedStrategy[np.where(PlannedStrategy[:,0]>t_now)]', PlannedStrategy[np.where(PlannedStrategy[:,0]>t_now)])
#exit()
#%% Collect Raw Data and Stages' geometry
X0well, X1well, HydraulicLogs_raw, catalog_incomplete_unbounded_df, StartTime = DK.Process_All_RAMSIS_Observations(QML_RealTime_Catalog, PATH_2_FIND_ONLY_JSON, MyJSONTimeConversion, MyColumns)
catalog_incomplete_df=catalog_incomplete_unbounded_df[catalog_incomplete_unbounded_df['ts']>0.]
catalog_incomplete_df=DK.Restrict_Catalog_To_Forecasting_Bounds(catalog_incomplete_df, Forecasts_Edges, ['Xs','Ys','Zs'])
HydraulicLogs_coarse_pd=DK.CoarsenWellLogs_Panda(HydraulicLogs_raw, columns=MyColumns, mean_dt_coarse=mean_dt_coarse)
#print('HydraulicLogs_course_pd being used for forecast input...', HydraulicLogs_coarse_pd)

#exit()
#%% Hard-code scenario
print('Next two lines need to be manually updated before running HM1')
Sea_Level_Depth_of_coordinates_center=(5000.-X1well[2]-259.3) # Dieter needs co-ordinates centered at the projection of the CSH to the surface


#%% set up necessary input from the user
my_Recenter=[0, 0., Sea_Level_Depth_of_coordinates_center] 
catalog_for_calibration=catalog_incomplete_df[catalog_incomplete_df['ts']<=t_now]
well_logs_for_calibration=HydraulicLogs_coarse_pd[MyColumns][HydraulicLogs_coarse_pd[MyColumns[0]]<=t_now]
# I have changed the below line so that the actual strategy is used, rather than the hydraulic logs
strategy_to_predict_from = PlannedStrategy[np.where(PlannedStrategy[:,0]>t_now)]
#strategy_to_predict_from = np.array(HydraulicLogs_coarse_pd[MyColumns])
my_Recenter=[0, 0., (5000.-X1well[2]-259.3)] # Dieter needs co-ordinates centered at the projection of the CSH to the surface #### what is CSH

#%% run the worker
Worker=Dieter.HM1(StartTime=StartTime, Recenter=my_Recenter)
Worker.maxiter=1  # Altered this!
Worker.fit(well_logs_for_calibration, catalog_for_calibration)
Worker_Forecast=Worker.predict(strategy_to_predict_from, Forecasts_Moments, Forecasts_Edges)

My_Returned_Forecast=np.array([Forecasts_Moments,Worker_Forecast[1],np.sum(Worker_Forecast[0], axis=(1,2,3))]). transpose()
np.save("Forecast_subgeometries_DK", Worker_Forecast[0])
np.savetxt(Output_Forecast, My_Returned_Forecast, delimiter=';', newline='\n', header='time;b_value_total;HM1_Forecast\n')


