"""
HM1 test for seed code with data
"""
import os
import json
import datetime as dt
import pandas as pd
import unittest
import numpy as np
from ramsis.sfm.hm1.core.hm1_model import exec_model
from ramsis.sfm.hm1.core import utils
from ramsis.sfm.hm1 import settings

ABS_PATH = os.path.dirname(os.path.realpath(__file__))
PARENT_ABS_PATH = os.path.dirname(ABS_PATH)

WELL_DATA = {"altitude_value": 0.0,
             "sections": [
                 {"toplatitude_value": 47.5861,
                  "toplongitude_value": 7.593645,
                  "topdepth_value": 0.0,  # Something not right in json files
                  "bottomlatitude_value": 47.586398,
                  "bottomlongitude_value": 7.593238,
                  "bottomdepth_value": 4740.3}]}


def hydraulics_obs_json(file_dir, matching_str='dataWoLimit',
                        format_str='%Y-%m-%dT%H:%M:%S',
                        columns=['time', 'flow', 'pressure'],
                        endtime=dt.datetime(2006, 12, 7, 6)):
    list_files = os.listdir(file_dir)
    hydraulics_df = pd.DataFrame(columns=columns)
    for json_file in list_files:
        full_path = os.path.join(file_dir, json_file)
        print("Reading json file: ", json_file)
        with open(full_path) as opened_file:
            well_data = json.load(opened_file)

        sections_data = well_data['sections']
        hydraulics_data = pd.DataFrame(sections_data[0]['hydraulics'])
        # This start time is wrong, should take the first time of the
        # hydraulics data.
        inj_start_time = dt.datetime.strptime(
            sections_data[0]['starttime'], format_str)

        flow_top = np.array(pd.io.json.json_normalize(
            np.array(hydraulics_data['topflow'])))
        flow_bottom = np.array(pd.io.json.json_normalize(
            np.array(hydraulics_data['bottomflow'])))

        # Is the below correct? Is a negative bottom flow
        flow_top[np.where(flow_top==0.)] = -flow_bottom[np.where(flow_top==0.)]
        pressure_top = np.array(pd.io.json.json_normalize(
            np.array(hydraulics_data['toppressure'])))

        dttime = np.array(pd.io.json.json_normalize(
            np.array(hydraulics_data['datetime'])))

        file_df = pd.DataFrame.from_records(
            data=np.array([dttime, flow_top, pressure_top]).transpose()[0],
            columns=columns, coerce_float=True)

        hydraulics_df = hydraulics_df.append(file_df)

    hydraulics_df['time'] = pd.to_datetime(
        hydraulics_df['time'], format=format_str)
    hydraulics_df.set_index('time', inplace=True)
    hydraulics_df = hydraulics_df[hydraulics_df.index <= endtime]
    inj_end_time = hydraulics_df.index.max().to_pydatetime()

    return hydraulics_df, sections_data, inj_start_time, inj_end_time

def read_catalog(catalog_file_path):
    catalog = utils.obspy_catalog_parser(catalog_file_path)
    print("after reading catalog:", len(catalog), catalog)
    return catalog

def read_hydraulics_plan(hydraulics_file_path, obs_start_time):
    """
    Read csv file containing two columns, time and flow, and
    return pandas dataframe.

    param hydraulics_file_path: absolute path to plan csv
    param obs_start_time: Time that the plan is due to start

    rtype: pandas dataframe
    """
    hydraulics = pd.read_csv(hydraulics_file_path, sep=' ',
                             usecols=['time', 'flow'])
    hydraulics['time'] = pd.to_timedelta(hydraulics['time'],
                                         unit='S') + obs_start_time
    hydraulics.set_index('time', inplace=True)

    return hydraulics

def filter_catalog(catalog, z_offset, enu_border, starttime, endtime):
    """
    Filter catalog to a simple box with the extent of enu_boarder
    in each direction, with the center of the box located at the
    easting, northing zero point in the catalog's local coordinates
    and +z_offset in the up direction. Also filters in time.

    param z_offset: focal location in meters above the zero point
        in local coordinates
    param enu_boarder: 0.5 * extent in easting, northing, up directions
    param starttime: filter seismic events before this time
    param endtime: filter seismic events after this time

    rtype: pandas dataframe
    """
    catalog = catalog[catalog['northing'] >= -enu_border]
    catalog = catalog[catalog['northing'] <= enu_border]
    catalog = catalog[catalog['easting'] >= -enu_border]
    catalog = catalog[catalog['easting'] <= enu_border]
    print("catalog after enu border", len(catalog))
    catalog = catalog[catalog['up'] >= -enu_border + z_offset]
    catalog = catalog[catalog['up'] <= enu_border + z_offset]
    print("catalog after enu depth filter", len(catalog))
    catalog = catalog[catalog.index >= starttime]
    catalog = catalog[catalog.index <= endtime]
    print("catalog after time filter", len(catalog))
    return catalog

class SeedTestCase(unittest.TestCase):

    def test_exec_code(self):
        """
        Compare the refactored model code with csv results output
        by old version of the code.
        """
        z_offset = 4364.4
        hydraulics_basename = 'Geldinganes_Planned_Injection'
        catalog_filename = 'cat-basel2006.qml'
        hydraulics_file_path = os.path.join(ABS_PATH,
                                            'test_data',
                                            hydraulics_basename)
        catalog_file_path = os.path.join(ABS_PATH,
                                         'test_data',
                                         catalog_filename)
        hyd_obs_dir = os.path.join(
            PARENT_ABS_PATH, 'RAMSIS_LAST_FETCH', 'json_file')

        (hydraulics,
         sections_data,
         obs_start_time,
         obs_end_time) = hydraulics_obs_json(hyd_obs_dir)

        hydraulics_plan = read_hydraulics_plan(hydraulics_file_path,
                                               obs_end_time)
        end_forecast = hydraulics_plan.index.max()
        catalog = read_catalog(catalog_file_path)
        catalog = utils.translate_catalog_local_coords(WELL_DATA, catalog)
        catalog['up'] = -catalog['up']
        print("after translate catalog", catalog)

        i_vec = np.linspace(-1000, 1000, 11)
        j_vec = np.linspace(-1000, 1000, 11)
        # Describe what is happening on next line more fully
        k_vec = np.linspace(-1000, 1000, 11) + z_offset
        forecast_edges = [i_vec, j_vec, k_vec]

        catalog = filter_catalog(catalog, z_offset,
                                 1000.0,
                                 obs_start_time,
                                 obs_end_time)
        well_radius = 0.118
        bin_size = 0.005
        test_mode = True
        # Need to check the ordering of these and compare to model adaptor.
        well_section_bottom = [0, 0, 4618.3]
        well_section_top = [-31, 33, 5000]
        default_settings = settings.RAMSIS_WORKER_SFM_DEFAULTS[
            'model_parameters']
        print("end times: ", obs_end_time, end_forecast)
        forecast_values = exec_model(
            catalog,
            hydraulics,
            hydraulics_plan,
            obs_end_time,
            7200,
            0.02,
            5,
            obs_end_time,
            end_forecast,
            30240,
            forecast_edges,
            1,  # number of differential_evolutions.
            -1, # number of diff evolution workers
            default_settings["hm1_seed_settings"],
            default_settings["hm1_external_solution_settings"],
            default_settings["hm1_caps_kd_settings"],
            bin_size,
            well_radius,
            well_section_top,
            well_section_bottom,
            test_mode,
            "Bin\GES.RAMSIS.SSM.Simulation.Main.exe",
            "C:\RAMSIS\Worker_New\Simulation_Test"
            )
        # forecast_output is the number of events.
        forecast_dates, forecast_output, total_b_value, mc, a = forecast_values

        # Save the data to files when updating the tests for
        # if the model code changes.
        # np.save(os.path.join(ABS_PATH, "forecast_sub_geometry_output"),
        #         forecast_subgeoms)
        # np.save(os.path.join(ABS_PATH, "forecast_output"), forecast)
        # np.save(os.path.join(ABS_PATH, "b_output"), b)
        # forecast_output = np.load(os.path.join(
        #    ABS_PATH, "forecast_output.npy"))
        # b_output = np.load(os.path.join(ABS_PATH, "b_output.npy"))
        with open(os.path.join(ABS_PATH, 'test_data', 'HM1_Forecast.csv'),
                  'r') as comp_file:
            comparison_df = pd.read_csv(comp_file, sep=';')
        forecast_comparison = np.load(os.path.join(ABS_PATH,
                                      "Forecast_subgeometries_DK.npy"))

        array_comparison = comparison_df.to_numpy()
        forecast_output = forecast_output[:len(comparison_df)]
        subgeom_normalized_diff = ((forecast_comparison - forecast_output)
                                   / forecast_comparison) # noqa
        subgeom_percentage_max = np.nanmax(subgeom_normalized_diff) * 100.
        self.assertTrue(subgeom_percentage_max, 0.15)

        # Find the percentage difference between the results and comparison
        # results, on the total reservoir level.
        forecast_normalized_diff = ((array_comparison[:, 2]
                                     - forecast_output.sum(axis=(1, 2, 3))) # noqa
                                    / array_comparison[:, 2]) # noqa
        forecast_percentage_max = np.nanmax(forecast_normalized_diff) * 100.
        # Allow a percentage difference of greater than zero as there are
        # small differences based on corrected inputs to code. 0.15%
        # chosen as all differences are less than this.
        self.assertTrue(forecast_percentage_max < 0.15)
        self.assertTrue(False)
