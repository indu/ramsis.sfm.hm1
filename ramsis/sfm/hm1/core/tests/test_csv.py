import os
import datetime
import unittest
from ramsis.sfm.hm1.core.create_csv_input import CSVInputMixin


ABS_PATH = os.path.dirname(os.path.realpath(__file__))
PARENT_ABS_PATH = os.path.dirname(ABS_PATH)
class TranslateCatalogTestCase(unittest.TestCase):

    def test_calibrate_csv(self):
        csv_input = CSVInputMixin()
        csv_input.hm1_settings = {
            "fraction_seismic_cloud": 1.85,
            "borehole_storage_coefficient": 0.02097,
            "borehole_transmissivity": 1000,
            "initial_storage_coeff": 3.4E-05,
            "initial_transmissivity": 7.43E-08,
            "time_discretization": 7200,
            "deltat": 60,
            "well_radius": 0.118,
            "mesh_extent": 1000,
            "alfa_rate_of_growth": 5,
            "size_first_element": 0.01}
        csv_input.time_reference = datetime.datetime(2019, 4, 14)
        tmp_dir = os.path.join(ABS_PATH, 'tmp')
        if not os.path.isdir(tmp_dir):
            os.mkdir(tmp_dir)
        csv_input.input_para_calibrate_press = os.path.join(
            tmp_dir, 'para_calibrate_press_test.csv')
        csv_input.input_para_forecast_press = os.path.join(
            tmp_dir, 'para_forecast_press_test.csv')
        csv_input.input_para_common = os.path.join(
            tmp_dir, 'para_common_test.csv')
        csv_input.catalog_pathname = 'catalog_pathname'
        csv_input.hydraulics_pathname = 'hydraulics_pathname'
        csv_input.planned_hydraulics_pathname = 'planned_hydraulics_pathname'
        csv_input.well_pressure_pathname = 'well_pressure_pathname'
        csv_input.tecplot_dir = 'tecplot_dir'
        csv_input.csv_dir = 'csv_dir'
        csv_input.data_exchange_dir = 'data_exchange_dir'
        csv_input.create_input_para_calibrate_press()
        csv_input.create_input_para_forecast_press()
        csv_input.create_input_para_common()
        with open(os.path.join(
            ABS_PATH, 'test_data', 'para_calibrate_press.csv'), 'r')\
                as calib_reference:
            calib_ref_read = calib_reference.read()
        with open(csv_input.input_para_calibrate_press, 'r') as calib_test:
            calib_test_read = calib_test.read()
        self.assertEqual(calib_test_read, calib_ref_read)

        with open(os.path.join(
            ABS_PATH, 'test_data', 'para_forecast_press.csv'), 'r')\
                as forecast_reference:
            forecast_ref_read = forecast_reference.read()
        with open(csv_input.input_para_forecast_press, 'r') as forecast_test:
            forecast_test_read = forecast_test.read()
        self.assertEqual(forecast_test_read, forecast_ref_read)

        with open(os.path.join(
            ABS_PATH, 'test_data', 'para_common.csv'), 'r')\
                as common_reference:
            common_ref_read = common_reference.read()
        with open(csv_input.input_para_common, 'r') as common_test:
            common_test_read = common_test.read()
        self.assertEqual(common_test_read, common_ref_read)
