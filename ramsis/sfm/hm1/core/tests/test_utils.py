import pandas as pd
import datetime
import unittest
import numpy as np


class TranslateCatalogTestCase(unittest.TestCase):

    def test_standard(self):
        well_data = {
            "altitude_value": 200,
            "sections": [
                {"toplatitude_value": 40.0,
                "toplongitude_value": 10.0,
                "topdepth_value": 0.0,
                "bottomlatitude_value": 40.0001,
                "bottomlongitude_value": 10.0001,
                "bottomdepth_value": 1000
                }]}
        date_start = datetime.datetime(2019, 1, 1)
        ndays = 7
        catalog = pd.DataFrame({'date': [
            date_start + datetime.timedelta(days=x)
            for x in range(ndays)],
                                'lat': [40 + 0.01 * i for i in range(ndays)],
                                'lon': [10 + 0.01 * i for i in range(ndays)],
                                'depth':[i * 1000 for i in range(ndays)]})
        catalog = catalog.set_index('date')
        injection_enu, catalog = utils.translate_catalog_local_coords(
                well_data, catalog)
        print(injection_enu, catalog)
        self.assertFalse(True)
