# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
Miscellaneous HM1 model core facilities.
"""
from datetime import timedelta
import pandas as pd
from obspy import read_events
import pymap3d as pm
import logging

from ramsis.sfm.hm1.core.error import (HM1ObspyCatalogError,
                                       HM1WellInputError)


# TODO (sarsonl) further checks on information matching between the catalog
# and the hydraulics. Potentially lat, lon, maybe datetime although this is
# tricky as valid use case can have no overlap if seismics are infrequent.

def obspy_catalog_parser(xml_string):
    if isinstance(xml_string, str):
        xml_string = xml_string.encode('utf-8')
    try:
        obspy_catalog = read_events(xml_string)
    except AttributeError:
        raise HM1ObspyCatalogError()
    # TODO (sarsonl) Should there be further checks on the information,
    # such as lat and lon?
    if len(obspy_catalog.events) == 0:
        raise HM1ObspyCatalogError("Number of events is zero.")
    (dttime_column,
     mag_column,
     latitude_column,
     longitude_column,
     depth_column) = zip(*[(e.preferred_origin().time.datetime,
                            e.preferred_magnitude().mag,
                            e.preferred_origin().latitude,
                            e.preferred_origin().longitude,
                            e.preferred_origin().depth)
                           for e in obspy_catalog.events])
    # sort_index() is required for the model. If the index is not
    # in order, it cannot be searched and sliced.
    catalog = pd.DataFrame({'Mw': mag_column,
                            'lat': latitude_column,
                            'lon': longitude_column,
                            'depth': depth_column},
                           index=dttime_column).sort_index()
    return catalog

def hydraulics_parser(well_data, scenario=False):
    try:
        # Assume only one section
        section = well_data['sections'][0]
        hydraulics_data = section['hydraulics']
    except AttributeError:
        raise HM1WellInputError("There are no hydraulics provided.")

    hole_diameter = section.get('holediameter_value')
    if not hole_diameter:
        raise HM1WellInputError("The hole diameter for the well section"
                                " used must be given")
    well_radius = hole_diameter / 2.0 if hole_diameter else None

    projected_lat = section['toplatitude_value']
    projected_long = section['toplongitude_value']
    print("projected long, lat in hyd", projected_long, projected_lat)
    projected_depth = 0.0
    local_top_coordinates= pm.geodetic2enu(
        section['toplatitude_value'],
        section['toplongitude_value'],
        section['topdepth_value'],
        projected_lat, projected_long, projected_depth)
    local_bottom_coordinates= pm.geodetic2enu(
        section['bottomlatitude_value'],
        section['bottomlongitude_value'],
        section['bottomdepth_value'],
        projected_lat, projected_long, projected_depth)
    if scenario:
        (dttime_column,
         flow_column) = zip(*[(h['datetime_value'],
                               h['topflow_value'])
                              for h in hydraulics_data])

        hydraulics = pd.DataFrame({'flow': flow_column},
                                  index=dttime_column).sort_index()
    else:
        (dttime_column,
         flow_column,
         pressure_column) = zip(*[(h['datetime_value'],
                                   h['topflow_value'],
                                   h['toppressure_value'])
                                  for h in hydraulics_data])

        hydraulics = pd.DataFrame({'flow': flow_column,
                                  'pressure': pressure_column},
                                  index=dttime_column).sort_index()

    return (hydraulics, well_radius, local_top_coordinates,
            local_bottom_coordinates)

def translate_catalog_local_coords(well_data, catalog):
    """
    Translate catalog with reference to the projection of the
    casing show (top of well section) onto the surface.
    """
    altitude = well_data['altitude_value']
    # Assume only one section
    section = well_data['sections'][0]
    projected_lat = section['toplatitude_value']
    projected_long = section['toplongitude_value']
    print("projected lon, lat", projected_long, projected_lat)
    print("lon lat from row0", catalog)
    projected_depth = -altitude

    # Translate catalog to corodinates relative to well head
    catalog[['easting', 'northing', 'up']] = catalog.apply(
        lambda row: pm.geodetic2enu(
            row['lon'], row['lat'], -row['depth'],
            projected_long, projected_lat, -projected_depth),
        axis=1, result_type="expand")
    catalog = catalog.drop(['lat', 'lon', 'depth'], axis=1)
    return catalog


class PolyhedralSurfaceFromVectors3D(object):
    """
    Conversion class for 3D array of coordinate values
    to Polyhedral surfaces that represent a volume defined
    by an array element and the surrounding array elements.
    """
    def __init__(self, i_vec, j_vec, k_vec):
        """
        :param coords_array: Array of coordinates representing
        subgeometry verticies
        :type coords_array: np.array 3d
        """
        self.logger = logging.getLogger()
        self.i_vec = i_vec
        self.j_vec = j_vec
        self.k_vec = k_vec

    def polyhedral_surface(self, i, j, k):
        """
        Return the polyhedral surface associated with the indicies
        given.

        :param i: Index of first dimension
        :param j: Index of second dimension
        :param k: Index of third dimension

        :rtype: str
        """
        try:
            n0=self.i_vec[i]
            e0=self.j_vec[j]
            d0=self.k_vec[k]
            n1=self.i_vec[i + 1]
            e1=self.j_vec[j + 1]
            d1=self.k_vec[k + 1]
        except IndexError as err:
            self.logger.exception(
                f"{err} Index given to polyhedral_surface "
                f"is out of range: {i}, {j}, {k} "
                f"shape: {self.i_vec}, {self.j_vec}, {self.k_vec}")
            raise
        ostring = (
            f"POLYHEDRALSURFACE Z ("
            f"(({n0} {e0} {d0},{n0} {e1} {d0},{n1} {e1} {d0},"
            f"{n1} {e0} {d0},{n0} {e0} {d0})),"
            f"(({n0} {e0} {d0},{n0} {e1} {d0},{n0} {e1} {d1},"
            f"{n0} {e0} {d1},{n0} {e0} {d0})),"
            f"(({n0} {e0} {d0},{n1} {e0} {d0},{n1} {e0} {d1},"
            f"{n0} {e0} {d1},{n0} {e0} {d0})),"
            f"(({n1} {e1} {d1},{n1} {e0} {d1},{n0} {e0} {d1},"
            f"{n0} {e1} {d1},{n1} {e1} {d1})),"
            f"(({n1} {e1} {d1},{n1} {e0} {d1},{n1} {e0} {d0},"
            f"{n1} {e1} {d0},{n1} {e1} {d1})),"
            f"(({n1} {e1} {d1},{n1} {e1} {d0},{n0} {e1} {d0},"
            f"{n0} {e1} {d1},{n1} {e1} {d1})))")
        return ostring

# Not sure if required
def daterange(start_date, end_date, epoch_duration=None):
    """Generator that yields datetimes with constant increment.

    :param st art_date: Datetime for generator to start with.
    :param end_date: Datetime for generator to finish with.
    :param epoch_duration: Number of seconds between returned values.
    :rtype: Generator.
    """
    total_seconds = (end_date - start_date).total_seconds()
    if not epoch_duration:
        epoch_duration = total_seconds
    # the +1 is required to match the r code which gives the range
    # including the final variable.
    for n in range(int(total_seconds/epoch_duration)+1):
        yield start_date + timedelta(seconds=n*epoch_duration)
