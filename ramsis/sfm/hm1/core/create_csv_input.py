import csv
import os
from datetime import timedelta


class CSVInputMixin:
    def __init__(self):
        self.input_para_calibrate_press = None
        self.input_para_forecast_press = None
        self.input_para_common = None
        self.hydraulics_pathname = None
        self.planned_hydraulics_pathname = None
        self.catalog_pathname = None
        self.well_pressure_pathname = None
        self.tecplot_dir = None
        self.csv_dir = None
        self.data_exchange_dir = None

    def create_input_para_calibrate_press(self):
        with open(self.input_para_calibrate_press, mode='w') as calibrate_para:
            calibrate_writer = csv.writer(calibrate_para, delimiter=';')
            calibrate_writer.writerow(
                ['ID_Path', 'Name', 'Value', 'Description', 'ID',
                 'Level_1', 'Level_2', 'Level_3'])
            calibrate_writer.writerow(
                ['InputParameter.InputData.file_pressure', 'file_pressure',
                 self.well_pressure_pathname, 'pressure time series', 1000,
                 'InputData', '', ''])
            calibrate_writer.writerow(
                ['InputParameter.InputData.file_mag', 'file_mag',
                 self.catalog_pathname, 'seismic catalog',
                 1000, 'InputData', '', ''])
            calibrate_writer.writerow(
                ['InputParameter.InputData.file_flowrate', 'file_flowrate',
                 self.hydraulics_pathname, 'flow rate time series',
                 1000, 'InputData', '', ''])

    def create_input_para_forecast_press(self):
        with open(self.input_para_forecast_press, mode='w') as forecast_para:
            forecast_writer = csv.writer(forecast_para, delimiter=';')
            forecast_writer.writerow(
                ['ID_Path', 'Name', 'Value', 'Description', 'ID', 'Level_1',
                 'Level_2', 'Level_3'])
            forecast_writer.writerow(
                ['InputParameter.InputData.file_flowrate', 'file_flowrate',
                 self.planned_hydraulics_pathname, 'flow rate time series',
                 '1000', 'InputData', '', ''])

    def create_input_para_common(self):
        with open(self.input_para_common, mode='w') as common_para:
            common_writer = csv.writer(common_para, delimiter=';')
            common_writer.writerow(
                ['ID_Path', 'Name', 'Value', 'Description' 'ID',
                 'Level_1', 'Level_2', 'Level_3'])
            common_writer.writerow(
                ['InputParameter.Output.time_Reference', 'time_Reference',
                 self.time_reference.strftime('%d.%m.%Y %H:%M:%S'),
                 'Reference time for output data', 1000, 'Output', '', ''])
            common_writer.writerow(
                ['InputParameter.Output.path_Debug_Tecplot',
                 'path_Debug_Tecplot', self.tecplot_dir,
                 'Path to Tecplot outputs', 1001, 'Output', '', ''])
            common_writer.writerow(
                ['InputParameter.Output.path_Debug_CSV', 'path_Debug_CSV',
                 self.csv_dir, 'Path to CSV outputs', 1002, 'Output',
                 '', ''])
            common_writer.writerow(
                ['InputParameter.Output.path_DataExchange',
                 'path_DataExchange', self.data_exchange_dir,
                 'Path to data exchange folder', 1003, 'Output', '', ''])
            common_writer.writerow(
                ['InputParameter.Output.export_PressCalibration',
                 'export_PressCalibration', 'True', 'Include FMD of catalog '
                 'in serialized SSM output', 1005, 'Output', '', ''])
            common_writer.writerow(
                ['InputParameter.Output.export_MeasuredInputData',
                 'export_MeasuredInputData', 'False', 'Include FMD of '
                 'catalog  in serialized SSM output', 1005, 'Output', '', ''])
            common_writer.writerow(
                ['InputParameter.MeshGeometry.ini_size', 'ini_size',
                 self.hm1_settings['size_first_element'],
                 'Size of first element [m]', 2, 'MeshGeometry', '', ''])
            common_writer.writerow(
                ['InputParameter.MeshGeometry.alfa', 'alfa',
                 self.hm1_settings['alfa_rate_of_growth'], 'Rate of growth '
                 'of element size', 3, 'MeshGeometry', '', ''])
            common_writer.writerow(
                ['InputParameter.MeshGeometry.extent', 'extent',
                 self.hm1_settings['mesh_extent'], 'Extent of the mesh '
                 'in meters [m]', 4, 'MeshGeometry', '', ''])
            common_writer.writerow(
                ['InputParameter.MeshGeometry.rw', 'rw',
                 self.hm1_settings['well_radius'], 'Well radius [m]', 5,
                 'MeshGeometry', '', ''])
            common_writer.writerow(
                ['InputParameter.TimeDiscretization.time_start',
                 'time_start', (self.time_reference - timedelta(days=100)).
                 strftime('%d.%m.%Y %H:%M:%S'), 'Start time of simulation',
                 26, 'TimeDiscretization', '', ''])
            common_writer.writerow(
                ['InputParameter.TimeDiscretization.time_end', 'time_end',
                 (self.time_reference + timedelta(days=200)).
                 strftime('%d.%m.%Y %H:%M:%S'), 'End time of simulation',
                 27, 'TimeDiscretization', '', ''])
            # Not entirely sure what delta t relates to, is it the
            # step in time expected between hydraulic samples?
            common_writer.writerow(
                ['InputParameter.TimeDiscretization.deltat', 'delta t',
                 self.hm1_settings['deltat'], 'Simulation time [s]', 6,
                 'TimeDiscretization', '', ''])
            common_writer.writerow(
                ['InputParameter.TimeDiscretization.tw', 'tw',
                 self.training_epoch_duration, 'Calibration time '
                 '(time window) [s]', 8, 'TimeDiscretization', '', ''])
            common_writer.writerow(
                ['InputParameter.InitialVal.trans_ini', 'trans_ini',
                 self.hm1_settings['initial_transmissivity'], 'Initial '
                 'transmissivity [m2/s]', 9, 'InitialVal', '', ''])
            common_writer.writerow(
                ['InputParameter.InitialVal.stor_ini', 'stor_ini',
                 self.hm1_settings['initial_storage_coeff'],
                 'Initial storage coeff', 10, 'InitialVal', '', ''])
            common_writer.writerow(
                ['InputParameter.InitialVal.trans_bore', 'trans_bore',
                 self.hm1_settings['borehole_transmissivity'],
                 'Borehole transmissivity [m2/s]', 11, 'InitialVal', '', ''])
            common_writer.writerow(
                ['InputParameter.InitialVal.stor_bore', 'stor_bore',
                 self.hm1_settings['borehole_storage_coefficient'],
                 'Borehole storage coefficient', 12, 'InitialVal', '', ''])
            common_writer.writerow(
                ['InputParameter.SeismicCloud.fraction_seismic_cloud',
                 # Not sure if missing d on cloud below is intentional
                 'fraction_seismic_clou',
                 self.hm1_settings['fraction_seismic_cloud'],
                 'Factor controlling the extent of the stim. zone', 21,
                 'SeismicCloud', '', ''])
            common_writer.writerow(
                ['InputParameter.MiscAndOutput.density', 'density', 970,
                 'Fluid density [kg/m3]', 22, 'MiscAndOutput', '', ''])

    def create_system_config_csv(self):
        with open(self.system_config, mode='w') as system_para:
            config_writer = csv.writer(system_para, delimiter=';')
            config_writer.writerow(
                ['ID_Path', 'Name', 'Value', 'Description' 'ID',
                 'Level_1', 'Level_2', 'Level_3'])
            config_writer.writerow(
                ['InputParameter.SystemConfig.path_tmp', 'path_tmp',
                 self.tmp_dir, 'Path to temporary directory', 1000,
                 'SystemConfig', '', ''])
            config_writer.writerow(
                ['InputParameter.SystemConfig.file_TRANSIN_EXE',
                 'file_TRANSIN_EXE',
                 os.path.join(self.hm1_base_dir, 'External_Bin',
                              'transin.exe'),
                 'File name for TRANSIN executable.',
                 1000, 'SystemConfig', '', ''])
            config_writer.writerow(
                ['InputParameter.SystemConfig.verbosity', 'verbosity', 3,
                 'Verbosity level.', 1000, 'SystemConfig', '', ''])
