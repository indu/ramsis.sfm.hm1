#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 29 14:59:42 2019

@author: dkarvoun@ethz.ch
"""
import numpy as np
from scipy.interpolate import CloughTocher2DInterpolator
from scipy.special import erf, erfinv

import pandas as pd
import json
from obspy import read_events
import pymap3d as pm
from datetime import datetime
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
from sklearn import linear_model
import os


class NarrayNormalSeeds:
    def __init__(self, Stresses, StressStd, Friction=0.6, Cohesion=0., StressDrop=3., Angle=[]):
        self.Stresses = Stresses
        self.StressStd = StressStd
        self.Friction=Friction
        self.Cohesion=Cohesion
        self.StressDrop=StressDrop
        self.Angle=Angle
        if (len(Angle)<1):
            self.Angle=np.arctan(Friction)/2.+np.pi/4.
        self.ThetaM=np.cos(self.Angle*2)-np.multiply(np.sin(self.Angle*2), np.reciprocal(self.Friction))
        self.Weights=(self.ThetaM*np.array([[1],[-1]])+1.)*0.5
        self.Pfmean=np.sum(self.Stresses*self.Weights.transpose(), axis=1)+self.Cohesion/self.Friction
        self.Pfstd=np.sqrt(np.sum(np.square(self.StressStd)*np.square(self.Weights.transpose()), axis=1))
        

        
def NormalCDF(X, Xmean=0, Xstd=1.):
    return (erf( (X-Xmean)/(np.sqrt(2.)*Xstd) ) +1.)*0.5

def NormalCDF_inv(X, Xmean=0, Xstd=1.):
    return ( erfinv(X*2.-1.)*np.sqrt(2)*Xstd + Xmean )

def TruncNormCDF(X, Xmean, Xstd, Xmin=-1.e20, Xmax=1.e20):
    Psi_a=NormalCDF((Xmin-Xmean)/Xstd)
    if (Xmin<Xmean+10.*Xstd):
        Psi_a=0.
    Psi_b=NormalCDF((Xmax-Xmean)/Xstd)
    if (Xmax>Xmean+10.*Xstd):
        Psi_b=1.
    CDF=(NormalCDF(X,Xmean, Xstd)-Psi_a)/(Psi_b-Psi_a)
    CDF[np.where(np.any([X<Xmin],axis=0))]=0
    CDF[np.where(np.any([X>Xmax],axis=0))]=1
    return CDF

def TruncNormCDF_inv(X, Xmean, Xstd, Xmin=-1.e20, Xmax=1.e20):
    Psi_a=NormalCDF((Xmin-Xmean)/Xstd)
    if (Xmin<Xmean+10.*Xstd):
        Psi_a=0.
    Psi_b=NormalCDF((Xmax-Xmean)/Xstd)
    if (Xmax>Xmean+10.*Xstd):
        Psi_b=1.
    return NormalCDF_inv(X*(Psi_b-Psi_a)+Psi_a ,Xmean, Xstd)

def From3DCartesian2_1DCylindrical(Xs, CenterTop, CenterBot, fill_value=np.nan):
    l1=np.sum((Xs-CenterBot)*(CenterTop-CenterBot),axis=1)/np.sum((CenterTop-CenterBot)**2,axis=1)
    l1=np.array([l1])
    Radius=np.linalg.norm(-Xs+np.transpose(l1).dot((CenterTop-CenterBot))+CenterBot,axis=1)
    for i in range(len(Radius)):
        if((l1[0,i]-1)*l1[0,i]> 0.):
            Radius[i]=fill_value
    return Radius

#def From3DCartesian2_2DCartesian(Xs, CenterTop, CenterBot, fill_value=np.nan):
#    l1=np.sum((Xs-CenterBot)*(CenterTop-CenterBot),axis=1)/np.sum((CenterTop-CenterBot)**2,axis=1)
#    l1=np.array([l1])
#    Radius=-Xs+np.transpose(l1).dot((CenterTop-CenterBot))+CenterBot
#    for i in range(len(Radius)):
#        if((l1[0,i]-1)*l1[0,i]> 0.):
#            Radius[i]=fill_value
#    return Radius
    

def Interpolator_Of_External_Pressure_Solution_Ptx (ExternalSolution, columns, UnitConversion, SkipRows):
        col_press=columns[0]
        col_time =columns[1]
        col_space=np.delete(columns, [0,1])
        PressCoeff=UnitConversion[0]
        TimeCoeff =UnitConversion[1]
        SpaceCoeff=np.delete(UnitConversion, [0,1])

        Tecplot_File = np.loadtxt(open(ExternalSolution, "rt"), delimiter=" ", skiprows=SkipRows)
        R_read=Tecplot_File[:,col_space]*SpaceCoeff  # convert spatial coordinates to meters
        t_read=Tecplot_File[:,col_time]*TimeCoeff   # convert time to seconds
        p_read=Tecplot_File[:,col_press]*PressCoeff  # convert pressure to Pa
        print("inputs to interpolator: ", p_read, np.array([R_read.ravel(), t_read]).T)
        My_CAPS_KD_Interpolator =CloughTocher2DInterpolator(np.array([R_read.ravel(),t_read]).T,p_read, fill_value=0)
        
        return My_CAPS_KD_Interpolator, np.unique(t_read)
    
def Interpolate_Maxima (InterpdFunction, My_Working_T, My_Working_X):
    Solution, MaxSolution =np.zeros([2, len(My_Working_T), len(My_Working_X)])
    TempSol=np.zeros(len(My_Working_X))
    for timeIter in range(len(My_Working_T)):
        ConsiderTime=My_Working_T[timeIter]
        Solution[timeIter,:]=InterpdFunction(My_Working_X,ConsiderTime)
        MaxSolution[timeIter,:]=np.maximum(TempSol,Solution[timeIter,:])
        TempSol=MaxSolution[timeIter,:] 
    return MaxSolution
#%%
def Refine_An_Edge (Coarse_Edges_1D, RR=5):
    Coarse_Cells=np.sort(np.unique(Coarse_Edges_1D.ravel()), kind='mergesort')
    MyFinerCells=np.zeros((len(Coarse_Cells)-1)*RR)
    for i in range(len(Coarse_Cells)-1):
        Delta=(Coarse_Cells[i+1]-Coarse_Cells[i])/RR
        for ii in range(RR):
            MyFinerCells[RR*i+ii]=Coarse_Cells[i]+Delta*(ii+0.5)
    return MyFinerCells

def Create_My_Grid(Forecasts_Edges, RefinementRatio=5):
    MyX_bins=Refine_An_Edge (np.unique(Forecasts_Edges[0].ravel()), RR=RefinementRatio)
    MyY_bins=Refine_An_Edge (np.unique(Forecasts_Edges[1].ravel()), RR=RefinementRatio)
    MyZ_bins=Refine_An_Edge (np.unique(Forecasts_Edges[2].ravel()), RR=RefinementRatio)
    My3DFineGrid=np.meshgrid(MyX_bins, MyY_bins, MyZ_bins,  indexing='ij')
    MyXYZ=np.array([My3DFineGrid[0].ravel(), My3DFineGrid[1].ravel(), My3DFineGrid[2].ravel()]).transpose()
    return MyXYZ


def Interpolate_Maximum_Pressure(Forecasts_Moments, Forecasts_Edges, External_Solution_Settings, CAPS_KD_Settings, pressure_file):
    My_PressureFunction, External_timesteps = Interpolator_Of_External_Pressure_Solution_Ptx(ExternalSolution=pressure_file, columns=External_Solution_Settings["FileCols"], UnitConversion=External_Solution_Settings["FileConvs"], SkipRows=External_Solution_Settings["SkipLines"])
    RR=CAPS_KD_Settings["RefinementRatio"]
    My_CartesianGrid=Create_My_Grid(Forecasts_Edges, RefinementRatio=RR)
    print("external solution settings",  type(External_Solution_Settings), External_Solution_Settings)
    Lw1=np.array([External_Solution_Settings["Lw1"]])
    Lw2=np.array([External_Solution_Settings["Lw2"]])
    print("after external, values of lw1 and lw2: ", Lw1, Lw2, My_CartesianGrid)
    MyR=From3DCartesian2_1DCylindrical(My_CartesianGrid, Lw1, Lw2)
    LaterKeepIndices=np.where(np.isfinite(MyR))
    My_Working_XYZ=My_CartesianGrid[LaterKeepIndices]
    My_Working_R  =MyR[LaterKeepIndices]
    if (CAPS_KD_Settings["EnableScrOutput"]):
        height=np.linalg.norm(Lw1-Lw2)
        normalVec=(Lw1-Lw2)/height
        print("An infinite cylindrical space is considered with depth ", height,"m. and is going to be mapped at the Target bins")
        print("The cylinder points towards the ",normalVec, "^T direction.")
        print("Warning: finding max pressure introduces numerical error due to discretization.")
    DT=np.concatenate((External_timesteps, Forecasts_Moments))
    MyT=np.sort(np.unique(DT))
    MyPressure, MaxPressureAll =np.zeros([2, len(MyT), len(My_Working_R)])
    PM=np.zeros(len(My_Working_R))
    for timeIter in range(len(MyT)):
        ConsiderTime=MyT[timeIter]
        MyPressure[timeIter,:]=My_PressureFunction(My_Working_R,ConsiderTime)
        MaxPressureAll[timeIter,:]=np.maximum(PM,MyPressure[timeIter,:])
        PM=MaxPressureAll[timeIter,:]
    TimeBinIndeces=np.intersect1d(Forecasts_Moments, MyT, return_indices=True)[2] # discard unecessary time discretization now    
    MaxPressure=MaxPressureAll[TimeBinIndeces]
    dV=np.unique(np.diff(Forecasts_Edges, axis=1), axis=1)
    if(len(dV)>3):
        print ("Warning: non equidistant edges have been passed in one direction. Correct array of control volumes.")
    #\todo: assume equidistant edges and refinement
    My_Cell_Volume=np.prod(np.amin(dV,axis=1)/RR)
    return MaxPressure, My_Working_XYZ, np.ones(len(My_Working_XYZ))*My_Cell_Volume

#%% Kernel Distribution for Converting Any Pressure to Seismicity
def Forecast_TotalEvents_Bvalue(Forecasts_Moments, Forecasts_Edges, MaxPressure, My_Working_XYZ, My_Cells_Volume, SeedsSettings):
    CAPS_Z=np.array([My_Working_XYZ[:,2]]).transpose()
    CAPS_Coh= SeedsSettings["cohesion_mean"]
    CAPS_fr=SeedsSettings["friction_mean"]
    CAPS_P0=SeedsSettings["min_failure_pressure"]
    CAPS_stressdrop_coeff=SeedsSettings["stressdrop_coeff"]
    CAP_dSdZ=np.array([[SeedsSettings["slope_sigma1"], SeedsSettings["slope_sigma3"]]])
    CAPS_Phydro=CAPS_Z*(SeedsSettings["gravity"]*985.*1.e-6)
    CAPS_MeanStresses=CAPS_Z*CAP_dSdZ+np.array([SeedsSettings["inter_sigma1"], SeedsSettings["inter_sigma3"]])
    CAPS_StdStress=CAPS_MeanStresses*np.array([SeedsSettings["sigma1_std"]/100., SeedsSettings["sigma3_std"]/100.])
    CAPS_Seeds=NarrayNormalSeeds(CAPS_MeanStresses, CAPS_StdStress, CAPS_fr, CAPS_Coh, CAPS_stressdrop_coeff)
    LaterKeepIndices=np.where(MaxPressure[-1]>=CAPS_P0)
    CAPS_Seismicity =np.zeros(MaxPressure.shape)
    P0=CAPS_P0
    Pfpp=CAPS_stressdrop_coeff/CAPS_fr
    for cell in LaterKeepIndices[0]:
        MPT=np.array([MaxPressure[:,cell]])
        First_Triggering=TruncNormCDF(MPT, CAPS_Seeds.Pfmean[cell]-CAPS_Phydro[cell], CAPS_Seeds.Pfstd[cell], Xmin =P0 )
        CAPS_Seismicity[:,cell]=First_Triggering[0]
        N_trigs=int(np.nanmax(MPT-P0)//(Pfpp))
        for i in range(N_trigs):
            First_Triggering+=TruncNormCDF(MPT, CAPS_Seeds.Pfmean[cell]-CAPS_Phydro[cell]+Pfpp*(i+1), CAPS_Seeds.Pfstd[cell], Xmin = P0+Pfpp*(i+1), Xmax=50.)
            CAPS_Seismicity[:,cell]=First_Triggering[0]
    Nt=len(Forecasts_Moments)
    TargetForecast_ExpectedEvents =np.zeros([Nt, len(Forecasts_Edges[0])-1, len(Forecasts_Edges[1])-1, len(Forecasts_Edges[2])-1])
    TargetForecast_ExpectedMaxMw  =TargetForecast_ExpectedEvents
    for t_i in range(Nt):
        TargetForecast_ExpectedEvents[t_i] ,e = np.histogramdd(My_Working_XYZ, bins=Forecasts_Edges, weights=CAPS_Seismicity[t_i,:]*My_Cells_Volume*SeedsSettings["PoissonDensity"])
#    TargetForecast_ExpectedEvents=TargetForecast_ExpectedEvents *SeedsSettings["PoissonDensity"]*My_Cell_Volume 
    print("Warning: Probabilistic verification of b-value averaging is pending.") 
    print("Check that now CAPS_Z can be exchanged by My_Working_XYZ")       
    CAPS_bvalue=My_Working_XYZ.dot(SeedsSettings["m"])+SeedsSettings["a"]
    CAPS_Mw=np.reciprocal(CAPS_bvalue)*np.log10(np.exp(1.))+SeedsSettings["Mag_Completeness"]
    Mw=(CAPS_Mw.transpose()).dot(CAPS_Seismicity.transpose())/np.sum(CAPS_Seismicity,axis=1)-SeedsSettings["Mag_Completeness"]
    TargetForecast_ExpectedMaxMw=np.log10(np.exp(1.))*(np.reciprocal(Mw))
    return CAPS_Seismicity, TargetForecast_ExpectedEvents, TargetForecast_ExpectedMaxMw.transpose()

def Forecast_BinnedRate_only(Forecasts_Moments, Forecasts_Edges, MaxPressure, My_Working_XYZ, My_Cells_Volume, SeedsSettings, NtrigMax=5):
    CAPS_Z=np.array([My_Working_XYZ[:,2]]).transpose()
    CAPS_Coh= SeedsSettings["cohesion_mean"]
    CAPS_fr=SeedsSettings["friction_mean"]
    CAPS_P0=SeedsSettings["min_failure_pressure"]
    CAPS_stressdrop_coeff=SeedsSettings["stressdrop_coeff"]
    CAP_dSdZ=np.array([[SeedsSettings["slope_sigma1"], SeedsSettings["slope_sigma3"]]])
    CAPS_Phydro=CAPS_Z*(SeedsSettings["gravity"]*985.*1.e-6)
    CAPS_MeanStresses=CAPS_Z*CAP_dSdZ+np.array([SeedsSettings["inter_sigma1"], SeedsSettings["inter_sigma3"]])
    CAPS_StdStress=CAPS_MeanStresses*np.array([SeedsSettings["sigma1_std"]/100., SeedsSettings["sigma3_std"]/100.])
    CAPS_Seeds=NarrayNormalSeeds(CAPS_MeanStresses, CAPS_StdStress, CAPS_fr, CAPS_Coh, CAPS_stressdrop_coeff)
    LaterKeepIndices=np.where(MaxPressure[-1]>=CAPS_P0)
    CAPS_Seismicity =np.zeros(MaxPressure.shape)
    P0=CAPS_P0
    Pfpp=CAPS_stressdrop_coeff/CAPS_fr
    for cell in LaterKeepIndices[0]:
        MPT=np.array([MaxPressure[:,cell]])
        First_Triggering=TruncNormCDF(MPT, CAPS_Seeds.Pfmean[cell]-CAPS_Phydro[cell], CAPS_Seeds.Pfstd[cell], Xmin =P0 )
        CAPS_Seismicity[:,cell]=First_Triggering[0]
        if(Pfpp==0.):
            continue
        N_trigs=min( NtrigMax , int(np.nanmax(MPT-P0)//(Pfpp)))
        for i in range(N_trigs):
            First_Triggering+=TruncNormCDF(MPT, CAPS_Seeds.Pfmean[cell]-CAPS_Phydro[cell]+Pfpp*(i+1), CAPS_Seeds.Pfstd[cell], Xmin = P0+Pfpp*(i+1), Xmax=50.)
            CAPS_Seismicity[:,cell]=First_Triggering[0]
    Nt=len(Forecasts_Moments)
    TargetForecast_ExpectedEvents =np.zeros([Nt, len(Forecasts_Edges[0])-1, len(Forecasts_Edges[1])-1, len(Forecasts_Edges[2])-1])
#    TargetForecast_ExpectedMaxMw  =TargetForecast_ExpectedEvents
    for t_i in range(Nt):
        TargetForecast_ExpectedEvents[t_i] ,e = np.histogramdd(My_Working_XYZ, bins=Forecasts_Edges, weights=CAPS_Seismicity[t_i,:]*My_Cells_Volume*SeedsSettings["PoissonDensity"])
#    TargetForecast_ExpectedEvents=TargetForecast_ExpectedEvents *SeedsSettings["PoissonDensity"]*My_Cell_Volume 
#    print("Warning: Probabilistic verification of b-value averaging is pending.") 
#    print("Check that now CAPS_Z can be exchanged by My_Working_XYZ")       
#    CAPS_bvalue=My_Working_XYZ*SeedsSettings["m"]+SeedsSettings["a"]
#    CAPS_Mw=np.reciprocal(CAPS_bvalue)*np.log10(np.exp(1.))+SeedsSettings["Mag_Completeness"]
#    Mw=(CAPS_Mw.transpose()).dot(CAPS_Seismicity.transpose())/np.sum(CAPS_Seismicity,axis=1)-SeedsSettings["Mag_Completeness"]
#    TargetForecast_ExpectedMaxMw=np.log10(np.exp(1.))*(np.reciprocal(Mw))
    return TargetForecast_ExpectedEvents
#%%
#def Exemplary_Configuration_Dictionaries():
#    SeedsSettings = {
#            "slope_sigma1":0.037,
#            "slope_sigma3":0.015,
#            "sigma1_std":10,
#            "sigma3_std":10,
#            "fluid_density":985,
#            "gravity":9.81,
#            "inter_sigma1":0,
#            "inter_sigma3":0,
#            "b_vs_depth_range":True,  # b=a+m*z                                                                                                            
#            "a":2.,
#            "m":[0, 0, -0.000167],
#            "Mag_Completeness":0.9,
#            "min_failure_pressure":	0.01,
#
#            "stressdrop_coeff": 3., 
#            "cohesion_mean":2,
#            "friction_mean":0.6,
#
#            "PoissonDensity":1.e-6
#            }
#
#    External_Solution_Settings ={
#            # Have moved all this to the settings file, but don't think that
#            #below is ever used
#            "FileNameSolutions":"./External_Files/Press_Dist_Calib.dat",
#            "FileCols":[2,0,1],
#            "FileConvs":[1.e-6,24.*3600., 1.],
#            "SkipLines":6,  
#            "Lw1":[0.,0.,5000.],
#            "Lw2":[-31., 33., 4618.3]
#            }
#    
#    CAPS_KD_Settings={
#            "RefinementRatio":5,
#            "EnableScrOutput":True
#            }
#    return    SeedsSettings,  External_Solution_Settings, CAPS_KD_Settings


#%%
def b_value_Lasso_calibration(Xs, Mw, bins=100):
    counts, edges =np.histogramdd(Xs, bins=bins)
    counts=counts.flatten()
    meanMw, edges =np.histogramdd(Xs, bins=edges, weights=Mw)
    meanMw=meanMw.flatten()
 

    BinsGrid=np.meshgrid(edges[0][:-1] + 0.5 * (edges[0][1:] - edges[0][:-1]),
                         edges[1][:-1] + 0.5 * (edges[1][1:] - edges[1][:-1]),
                         edges[2][:-1] + 0.5 * (edges[2][1:] - edges[2][:-1]) )

    X=np.array([BinsGrid[0].flatten(),BinsGrid[1].flatten(),BinsGrid[2].flatten()])

    Keep=np.where(counts>0)
    X=X[:,Keep[0]].transpose()
    Y=meanMw[Keep]*np.reciprocal(counts[Keep])
    
    clf = linear_model.Lasso(alpha=0.1)
    clf.fit(X,Y)
    return clf.intercept_, clf.coef_

##%%
#from scipy import special
#class Eszter_LL_score:
#    def __init__(self, X , bins=10):
#        self.y, self.edges=np.histogramdd(X,bins=bins, density=True)
#        self.logyfact=np.log(special.factorial(self.y))
#
#    def score(self, X):
#        y1=np.histogramdd(X, bins=self.edges, density=True)
#        LL=self.y*np.log(y1)-y1-self.logyfact
#        return np.sum(LL)
#%%
        
#def CAPS_score(my_LL, Past_Moments, Past_Edges, Past_MaxPressure, My_Working_XYZ, My_Cells_Volume, SeedsSettings):
#    NormalizedBinnedSeismicity, TotalEvents, TotalBvalue=Forecast_TotalEvents_Bvalue(LL.edges[0], LL.edges[1:4], Past_MaxPressure, My_Working_XYZ, My_Cells_Volume, SeedsSettings)
#    F=my_LL.score
        
    

